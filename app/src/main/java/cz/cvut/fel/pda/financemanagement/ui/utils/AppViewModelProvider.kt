package cz.cvut.fel.pda.financemanagement.ui.utils

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import cz.cvut.fel.pda.financemanagement.FinanceManagementApplication
import cz.cvut.fel.pda.financemanagement.ui.screens.accounts.AccountDetailViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.accounts.AccountsViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.category.CategoryDetailViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.category.CategoryViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.loans.LoanDetailViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.loans.LoanViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.preferences.PreferencesViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.transactions.TransactionDetailViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.transactions.TransactionViewModel

object AppViewModelProvider {
    val Factory = viewModelFactory {
        initializer {
            TransactionViewModel(
                financemanagementApplication().container.transactionRepository
            )
        }
        initializer {
            TransactionDetailViewModel(
                financemanagementApplication().container.transactionRepository,
                financemanagementApplication().container.accountRepository,
                this.createSavedStateHandle()
            )
        }
        initializer {
            CategoryViewModel(
                financemanagementApplication().container.categoryRepository,
            )
        }
        initializer {
            CategoryDetailViewModel(
                financemanagementApplication().container.categoryRepository,
                this.createSavedStateHandle()
            )
        }
        initializer {
            PreferencesViewModel(
                financemanagementApplication().container.preferencesRepository
            )
        }
        initializer {
            LoanViewModel(
                financemanagementApplication().container.loanRepository
            )
        }
        initializer {
            LoanDetailViewModel(
                financemanagementApplication().container.loanRepository,
                this.createSavedStateHandle()
            )
        }
        initializer {
            AccountsViewModel(
                financemanagementApplication().container.accountRepository,
                financemanagementApplication().container.preferencesRepository
            )
        }
        initializer {
            AccountDetailViewModel(
                financemanagementApplication().container.accountRepository,
                this.createSavedStateHandle()
            )
        }
    }
}

fun CreationExtras.financemanagementApplication(): FinanceManagementApplication =
    (this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as FinanceManagementApplication)