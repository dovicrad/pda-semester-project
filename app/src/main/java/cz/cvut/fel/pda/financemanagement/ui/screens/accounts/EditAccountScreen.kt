package cz.cvut.fel.pda.financemanagement.ui.screens.accounts

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Check
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.database.account.Account
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.ui.components.BottomNavigation
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.components.TopBar
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider
import java.math.BigDecimal

@Composable
fun EditAccountScreen(
    navController: NavHostController,
    accountId: Long?,
    accountDetailViewModel: AccountDetailViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    ),
) {
    val uiState by accountDetailViewModel.uiState.collectAsStateWithLifecycle()

    when (val state = uiState) {
        is AccountDetailUiState.Error -> {
            ShowErrorScreen(
                errorMessage = state.exception.message ?: "Error",
                onClick = { navController.popBackStack() }
            )
        }
        AccountDetailUiState.Loading -> {
            ShowLoadingScreen()
        }
        is AccountDetailUiState.Success -> {
            ShowAccountSuccessScreen(
                state.account,
                navController,
                accountDetailViewModel
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ShowAccountSuccessScreen(
    account: Account,
    navController: NavHostController,
    accountDetailViewModel: AccountDetailViewModel
) {
    val name = rememberSaveable { mutableStateOf(account.name) }
    val balance = rememberSaveable { mutableStateOf(account.balance.toString()) }
    val currency = rememberSaveable { mutableStateOf(account.currency) }
    val currencies = OperationCurrency.entries
    val expanded = remember { mutableStateOf(false) }

    Scaffold(
        topBar = { TopBar(navController = navController) },
        content = { innerPadding ->
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(24.dp, 4.dp)
                    .verticalScroll(rememberScrollState())
                    .padding(innerPadding),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                val spaceModifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
                Text(
                    text = if (account.id == 0L) "New Account" else "Edit Account",
                    style = MaterialTheme.typography.titleLarge
                )
                OutlinedTextField(
                    value = name.value,
                    onValueChange = { name.value = it },
                    label = { Text("Name") },
                    modifier = spaceModifier
                )
                Box(modifier = spaceModifier) {
                    OutlinedTextField(
                        value = currency.value.name,
                        onValueChange = {},
                        label = { Text("Currency") },
                        readOnly = true,
                        trailingIcon = {
                            IconButton(onClick = { expanded.value = true }) {
                                Icon(Icons.Outlined.Check, contentDescription = "Choose Currency")
                            }
                        }
                    )
                    DropdownMenu(
                        expanded = expanded.value,
                        onDismissRequest = { expanded.value = false }
                    ) {
                        currencies.forEach { currencyOption ->
                            DropdownMenuItem(
                                onClick = {
                                    currency.value = currencyOption
                                    expanded.value = false
                                },
                                text = {
                                    Text(currencyOption.name)
                                }
                            )
                        }
                    }
                }
            }
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    accountDetailViewModel.saveAccount(
                        name = name.value,
                        balance = if (account.id == 0L) BigDecimal.ZERO else BigDecimal(balance.value),
                        currency = currency.value
                    )
                    navController.popBackStack()
                }
            ) {
                Icon(
                    Icons.Outlined.Check,
                    contentDescription = "Save",
                    modifier = Modifier.size(24.dp)
                )
            }
        },
        bottomBar = {
            BottomNavigation(navController = navController)
        }
    )
}
