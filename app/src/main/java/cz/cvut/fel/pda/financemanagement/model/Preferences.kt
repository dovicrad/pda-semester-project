package cz.cvut.fel.pda.financemanagement.model

import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency

data class Preferences(
    val preferredCurrency: OperationCurrency = OperationCurrency.CZK,
    val sendNotifications: Boolean = false
)
