package cz.cvut.fel.pda.financemanagement.ui.screens.category

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.FinanceManagementScreens
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.ui.components.BottomNavigation
import cz.cvut.fel.pda.financemanagement.ui.components.CreateDrawerContent
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.transactions.CreateTopBar
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider
import java.util.Locale

@Composable
fun CategoriesScreen(
    navController: NavHostController,
    categoryViewModel: CategoryViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    )
){
    val categoriesUiState: CategoriesUiState by categoryViewModel.categories.collectAsStateWithLifecycle()

    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()

    ModalNavigationDrawer(drawerContent = { CreateDrawerContent(navController, drawerState, scope) }, drawerState = drawerState) {
        Scaffold(
            topBar = { CreateTopBar(scope, drawerState) },
            content = {
                    innerPadding ->
                when (categoriesUiState) {
                    is CategoriesUiState.Loading -> {
                        ShowLoadingScreen()
                    }
                    is CategoriesUiState.Error -> {
                        ShowErrorScreen(
                            errorMessage = (categoriesUiState as CategoriesUiState.Error).exception.message ?: "Error",
                        )
                    }
                    is CategoriesUiState.Success -> {
                        CategoriesScreenContent((categoriesUiState as CategoriesUiState.Success).itemList,navController, innerPadding)
                    }
                }
            },
            floatingActionButton = {
                FloatingActionButton(onClick = {navController.navigate("${FinanceManagementScreens.EditCategory.name}/0")}) {
                    Icon(Icons.Default.Add, contentDescription = "Add")
                }
            },
            bottomBar = {
                BottomNavigation(navController = navController)
            }
        )
    }
}

@Composable
fun CategoriesScreenContent(categories: List<Category>, navController: NavHostController, innerPadding: PaddingValues) {
    val expenseCategories = categories.filter { it.type == CategoryType.EXPENSE }
    val incomeCategories = categories.filter { it.type == CategoryType.INCOME }

    Column(modifier = Modifier.padding(innerPadding).padding(horizontal = 14.dp)) {
        Text(text = "Expense Categories:", style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold))
        HorizontalDivider()
        CategoriesList(expenseCategories, navController)
        Text(text = "Income Categories:", style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold))
        HorizontalDivider()
        CategoriesList(incomeCategories, navController)
    }
}

@Composable
fun CategoriesList(categories: List<Category>, navController: NavHostController) {
    LazyColumn {
        items(categories) { category ->
            CategoryItem(category,navController)
        }
    }
}

@Composable
fun CategoryItem(category: Category, navController: NavHostController) {
    var showMenu by remember { mutableStateOf(false) }

    Row(
        modifier = Modifier.padding(8.dp).clickable { navController.navigate("${FinanceManagementScreens.CategoryDetail}/${category.id}") },
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = category.name.lowercase().replaceFirstChar { it.titlecase(Locale.ROOT) },
            modifier = Modifier.weight(1f)
        )
        Box(modifier = Modifier.weight(1f)) {
            IconButton(onClick = { showMenu = true }) {
                Icon(Icons.Filled.MoreVert, contentDescription = "Options")
            }
            DropdownMenu(
                expanded = showMenu,
                onDismissRequest = { showMenu = false }
            ) {
                DropdownMenuItem(onClick = {
                    navController.navigate("${FinanceManagementScreens.EditCategory.name}/${category.id}")
                    showMenu = false
                }, text = { Text("Edit") }
                )
                DropdownMenuItem(onClick = {
                    // Handle delete transaction here
                    // transactionViewModel.deleteTransaction(transaction.id)
                    showMenu = false
                }, text = { Text("Delete")})
            }
        }
    }
}
