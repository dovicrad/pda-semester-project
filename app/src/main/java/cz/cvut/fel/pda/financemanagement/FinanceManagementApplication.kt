package cz.cvut.fel.pda.financemanagement

import android.app.Application
import cz.cvut.fel.pda.financemanagement.di.AppContainer
import cz.cvut.fel.pda.financemanagement.di.AppDataContainer


class FinanceManagementApplication : Application() {
    lateinit var container: AppContainer
    override fun onCreate() {
        super.onCreate()
        container = AppDataContainer(this)
    }
}