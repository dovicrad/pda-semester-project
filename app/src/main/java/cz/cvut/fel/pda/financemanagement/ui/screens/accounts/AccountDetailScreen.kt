package cz.cvut.fel.pda.financemanagement.ui.screens.accounts

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.FinanceManagementScreens
import cz.cvut.fel.pda.financemanagement.database.account.Account
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.Transaction
import cz.cvut.fel.pda.financemanagement.ui.components.BottomNavigation
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.components.TopBar
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider

@Composable
fun AccountDetailScreen(
    navController: NavHostController,
    accountId: Long?,
    accountDetailViewModel: AccountDetailViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    ),
) {
    accountDetailViewModel.setAccountId(accountId ?: 0L)
    val uiState by accountDetailViewModel.uiState.collectAsStateWithLifecycle()

    Scaffold(
        topBar = { TopBar(navController) },
        content = { innerPadding ->
            when (val state = uiState) {
                is AccountDetailUiState.Error -> {
                    ShowErrorScreen(
                        errorMessage = state.exception.message ?: "Error",
                        onClick = { navController.popBackStack() }
                    )
                }
                AccountDetailUiState.Loading -> {
                    ShowLoadingScreen()
                }
                is AccountDetailUiState.Success -> {
                    AccountScreenContent(
                        state.account,
                        state.transactionsWithCategory,
                        innerPadding,
                        navController
                    )
                }
            }
        },
        floatingActionButton = {
            FloatingActionButton(onClick = {
                navController.navigate("${FinanceManagementScreens.EditAccount.name}/${accountId}")
            }) {
                Icon(Icons.Default.Edit, contentDescription = "Edit Account")
            }
        },
        bottomBar = { BottomNavigation(navController = navController) }
    )
}

@Composable
fun AccountScreenContent(
    account: Account,
    transactionsWithCategory: Map<Transaction, Category>,
    innerPadding: PaddingValues,
    navController: NavHostController
) {
    Column(modifier = Modifier.padding(innerPadding).padding(16.dp)) {
        Text(text = "Account Details", style = MaterialTheme.typography.titleLarge)
        Spacer(modifier = Modifier.height(16.dp))
        Text(text = account.name, style = MaterialTheme.typography.titleMedium)
        Text(text = account.balance.toString(), color = MaterialTheme.colorScheme.error, style = MaterialTheme.typography.titleSmall)
        Spacer(modifier = Modifier.height(16.dp))
        Text(text = "Transactions", style = MaterialTheme.typography.bodyLarge)
        HorizontalDivider()
        transactionsWithCategory.forEach { (transaction, category) ->
            TransactionItem(transaction, category)
        }
    }
}

@Composable
fun TransactionItem(transaction: Transaction, category: Category) {
    Row(modifier = Modifier.fillMaxWidth().padding(vertical = 8.dp), horizontalArrangement = Arrangement.SpaceBetween) {
        Column {
            Text(text = category.name, style = MaterialTheme.typography.bodyLarge)
            Text(
                text = transaction.amount.toString(),
                color = if (category.type == CategoryType.INCOME) Color.Green else Color.Red,
                style = MaterialTheme.typography.bodyLarge
            )
        }
        Text(text = transaction.dateCreated.toString(), style = MaterialTheme.typography.bodyMedium)
    }
}
