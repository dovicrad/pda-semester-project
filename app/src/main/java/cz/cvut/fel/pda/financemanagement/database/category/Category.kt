package cz.cvut.fel.pda.financemanagement.database.category

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "category")
data class Category(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val name: String,
    val type: CategoryType,
)

enum class CategoryType {
    EXPENSE,
    INCOME,
}
