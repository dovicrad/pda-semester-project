package cz.cvut.fel.pda.financemanagement.database.account

import androidx.room.Entity
import androidx.room.PrimaryKey
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import java.math.BigDecimal

@Entity(tableName = "account")
data class Account(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    val name: String,
    val balance: BigDecimal,
    val currency: OperationCurrency
)
