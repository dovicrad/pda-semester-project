package cz.cvut.fel.pda.financemanagement.database.category

import cz.cvut.fel.pda.financemanagement.model.CategoryWithTransactions
import kotlinx.coroutines.flow.Flow

class OfflineCategoryRepository(private val CategoryDao: CategoryDao):
    CategoryRepository {
    override fun getAllCategoriesStream(): Flow<List<Category>> = CategoryDao.getAllCategories()
    override fun getCategoryForIdStream(id: Long): Flow<Category?> = CategoryDao.getCategoryForId(id)
    override fun getCategoryWithTransactionsForIdStream(id: Long): Flow<CategoryWithTransactions?> {
        return CategoryDao.getCategoryWithTransactionsForId(id)
    }

    override suspend fun insertCategories(vararg categories: Category) = CategoryDao.insertCategories(*categories)
    override suspend fun deleteCategory(category: Category) = CategoryDao.delete(category)
    override suspend fun deleteAllCategories() = CategoryDao.deleteAll()
    override suspend fun updateCategory(vararg category: Category) = CategoryDao.updateCategories(*category)
}