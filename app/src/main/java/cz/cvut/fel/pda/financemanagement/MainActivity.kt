package cz.cvut.fel.pda.financemanagement

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import cz.cvut.fel.pda.financemanagement.ui.theme.FinanceManagementTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createNotificationChannel()
        setContent {
            FinanceManagementTheme {
               FinanceManagementApp()
            }
        }
    }
    private fun createNotificationChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Loan Notifications"
            val descriptionText = "Notifications about unreturned loans"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("cz.cvut.fel.pda.financemanagement", name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}