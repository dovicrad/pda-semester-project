package cz.cvut.fel.pda.financemanagement.ui.utils

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import cz.cvut.fel.pda.financemanagement.model.CurrencyResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val BASE_URL = "https://api.frankfurter.app"

private val retrofit = Retrofit.Builder()
    .baseUrl(BASE_URL)
    .addConverterFactory(JacksonConverterFactory.create(jacksonObjectMapper()))
    .build()

interface CurrencyExchangeClient {

    @GET("/latest")
    suspend fun getCurrency(@Query("from") currency: String) : Response<CurrencyResponse>

    companion object {
        @Volatile
        private var INSTANCE: CurrencyExchangeClient? = null

        fun getCurrencyExchangeClient(): CurrencyExchangeClient {
            return INSTANCE ?: synchronized(this) {
                val instance = retrofit.create(CurrencyExchangeClient::class.java)
                INSTANCE = instance
                instance
            }
        }
    }
}