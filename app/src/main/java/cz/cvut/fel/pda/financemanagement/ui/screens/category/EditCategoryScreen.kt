package cz.cvut.fel.pda.financemanagement.ui.screens.category

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Check
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SegmentedButton
import androidx.compose.material3.SegmentedButtonDefaults
import androidx.compose.material3.SingleChoiceSegmentedButtonRow
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.ui.components.BottomNavigation
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.components.TopBar
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider
import java.util.Locale

@Composable
fun EditCategoryScreen(navController: NavHostController,
                          categoryId: Long?,
                          categoryDetailViewModel: CategoryDetailViewModel = viewModel(
                              factory = AppViewModelProvider.Factory
                          ),){
    val uiState by categoryDetailViewModel.uiState.collectAsStateWithLifecycle()

    when (val state = uiState) {
        is CategoryDetailUiState.Error -> {
            ShowErrorScreen(
                errorMessage = state.exception.message ?: "Error",
                onClick = { navController.popBackStack() }
            )
        }
        CategoryDetailUiState.Loading -> {
            ShowLoadingScreen()
        }
        is CategoryDetailUiState.Success -> {
            ShowCategorySuccessScreen(
                state.category.category,
                navController,
                categoryDetailViewModel
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ShowCategorySuccessScreen(
    category: Category,
    navController: NavHostController,
    categoryDetailViewModel: CategoryDetailViewModel
) {
    val type = rememberSaveable { mutableStateOf(category.type) }
    val name = rememberSaveable { mutableStateOf(category.name) }
    val options = listOf(CategoryType.INCOME, CategoryType.EXPENSE)
    val selectedOption = remember { mutableStateOf(type.value) }


    Scaffold(
        topBar = {
            TopBar(navController = navController)
        },
        content = {
            innerPadding ->
            Column (
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(24.dp, 4.dp)
                    .verticalScroll(rememberScrollState())
                    .padding(innerPadding),

            ) {
                val spaceModifier = Modifier
                    .padding(vertical = 14.dp)
                Text(
                    modifier = spaceModifier,
                    style = MaterialTheme.typography.titleLarge.copy(fontWeight = FontWeight.Bold),
                    color = Color.Black,
                    text = if (category.id == 0L) "New Category" else "Edit Category")
                SingleChoiceSegmentedButtonRow{
                    options.forEachIndexed{index, option ->
                        SegmentedButton(
                            selected = selectedOption.value == option,
                            onClick = {
                                selectedOption.value = option
                                type.value = option
                            },
                            shape = SegmentedButtonDefaults.itemShape(index=index ,count = options.size)
                        ) {
                            Text(text = option.name.lowercase(Locale.getDefault()).replaceFirstChar { it.titlecase(Locale.getDefault()) })
                        }
                    }
                }
                HorizontalDivider(modifier = spaceModifier)
                OutlinedTextField(
                    value = name.value,
                    onValueChange = { newValue ->
                        name.value = newValue
                    },
                    label = { Text("name") }
                )
            }
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = { categoryDetailViewModel.saveCategory(
                name = name.value,
                type = type.value
            )
                navController.popBackStack() })
            {
                Icon(
                    Icons.Outlined.Check,
                    contentDescription = "Save",
                    modifier = Modifier.size(24.dp)
                )
            }
        },
        bottomBar = {
            BottomNavigation(navController = navController)
        }
    )
}
@Composable
fun EditTransactionDetailSuccessContent(
    innerPadding: PaddingValues,
    type: MutableState<CategoryType>, // Specify the correct type
    name: MutableState<String>
) {
    val openDialog = remember { mutableStateOf(false) }

    Column (
        modifier = Modifier
            .fillMaxWidth()
            .verticalScroll(rememberScrollState())
            .padding(innerPadding), // use innerPadding here
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val spaceModifier = Modifier
            .fillMaxWidth()
            .padding(24.dp, 4.dp)
        OutlinedTextField(
            value = name.value, // Use the correct variable
            onValueChange = { newValue ->
                name.value = newValue // Use the correct variable
            },
            label = { Text("name") }, // Use the correct label
            modifier = spaceModifier
        )
        // Add OutlinedTextField for date and dropdown menu for category here
    }
}