package cz.cvut.fel.pda.financemanagement.database.operation.loan

import androidx.room.Entity
import androidx.room.PrimaryKey
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.database.operation.Operation
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import java.math.BigDecimal
import java.time.LocalDateTime


@Entity(tableName = "loan")
data class Loan(
    @PrimaryKey(autoGenerate = true)
    override var id: Long = 0,
    override var description: String = "",
    override var amount: BigDecimal = BigDecimal.ZERO,
    override var dateCreated: LocalDateTime = LocalDateTime.now(),
    override var currency: OperationCurrency = OperationCurrency.CZK,
    var recipient : String = "",
    var isReturned : Boolean = false,
    var returnDate : LocalDateTime = LocalDateTime.now().plusDays(7),
    var type : CategoryType = CategoryType.EXPENSE
) : Operation()
