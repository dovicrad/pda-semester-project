package cz.cvut.fel.pda.financemanagement.ui.screens.accounts

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fel.pda.financemanagement.database.account.Account
import cz.cvut.fel.pda.financemanagement.database.account.AccountRepository
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.Transaction
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.math.BigDecimal

class AccountDetailViewModel(
    private val accountRepository: AccountRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    private var accountId: Long = savedStateHandle.get<Long>("Id")!!

    val uiState: StateFlow<AccountDetailUiState> =
        accountRepository.getAccountForIdStream(accountId)
            .combine(accountRepository.getTransactionsWithCategoryForAccount(accountId)) { account, transactionsWithCategory ->
                if (account != null) {
                    AccountDetailUiState.Success(account, transactionsWithCategory)
                } else if (accountId == 0L) {
                    AccountDetailUiState.Success(
                        Account(
                            id = 0,
                            name = "",
                            balance = BigDecimal.ZERO,
                            currency = OperationCurrency.CZK
                        ),
                        emptyMap()
                    )
                } else {
                    AccountDetailUiState.Error(Exception("Account not found"))
                }
            }.stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5000L),
                initialValue = AccountDetailUiState.Loading
            )

    fun setAccountId(id: Long) {
        accountId = id
    }

    fun saveAccount(
        name: String,
        balance: BigDecimal,
        currency: OperationCurrency
    ) {
        viewModelScope.launch {
            val account = Account(
                id = accountId,
                name = name,
                balance = balance,
                currency = currency
            )
            if (accountId == 0L) {
                accountRepository.insertAccounts(account)
            } else {
                accountRepository.updateAccounts(account)
            }
        }
    }
}

sealed interface AccountDetailUiState {
    data object Loading : AccountDetailUiState
    data class Success(
        val account: Account,
        val transactionsWithCategory: Map<Transaction, Category>
    ) : AccountDetailUiState
    data class Error(val exception: Exception) : AccountDetailUiState
}
