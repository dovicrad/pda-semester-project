package cz.cvut.fel.pda.financemanagement.database.operation.transaction

import cz.cvut.fel.pda.financemanagement.model.CategoryWithTransactions
import cz.cvut.fel.pda.financemanagement.model.TransactionWithCategory
import kotlinx.coroutines.flow.Flow

interface TransactionRepository {
    fun getAllTransactionsStream(): Flow<List<Transaction>>
    fun getTransactionForIdStream(id: Long): Flow<Transaction?>
    fun getTransactionWithCategoryForIdStream(id: Long): Flow<TransactionWithCategory>

    fun getCategoryWithTransactions(): Flow<List<CategoryWithTransactions>>
    fun getTransactionsWithCategory(): Flow<List<TransactionWithCategory>>
    suspend fun insertTransactions(vararg transactions: Transaction)
    suspend fun deleteTransaction(transaction: Transaction)
    suspend fun deleteAllTransactions()
    suspend fun deleteTransactionsByAccountId(accountId: Long)
    suspend fun updateTransactions(vararg transactions: Transaction)
}