package cz.cvut.fel.pda.financemanagement.database.operation.transaction

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import cz.cvut.fel.pda.financemanagement.model.CategoryWithTransactions
import cz.cvut.fel.pda.financemanagement.model.TransactionWithCategory
import kotlinx.coroutines.flow.Flow

@Dao
interface TransactionDao {

    @Query("SELECT * FROM `transaction` ORDER BY id ASC")
    fun getAllTransactions(): Flow<List<Transaction>>

    @Query("SELECT * FROM `transaction` WHERE id = :id")
    fun getTransactionForId(id: Long): Flow<Transaction>

    @androidx.room.Transaction
    @Query("""
    SELECT * FROM `category`
    JOIN `transaction` ON `transaction`.categoryId = `category`.id
    WHERE `transaction`.id = :id
""")
    fun getCategoryWithTransactionForId(id: Long): Flow<TransactionWithCategory>

    @androidx.room.Transaction
    @Query("""
    SELECT * FROM `category`
    JOIN `transaction` ON `transaction`.categoryId = `category`.id
""")
    fun getTransactionWithCategory(): Flow<List<TransactionWithCategory>>

    @androidx.room.Transaction
    @Query("""
    SELECT * FROM `category`
    ORDER BY `category`.id ASC
""")
    fun getCategoryWithTransaction(): Flow<List<CategoryWithTransactions>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTransactions(vararg transactions: Transaction)

    @Update
    suspend fun updateTransactions(vararg transactions: Transaction)

    @Delete
    suspend fun delete(item: Transaction)

    @Query("DELETE FROM `transaction`")
    suspend fun deleteAll()

    @Query("DELETE FROM `transaction` WHERE accountId = :accountId")
    suspend fun deleteTransactionsByAccountId(accountId: Long)
}
