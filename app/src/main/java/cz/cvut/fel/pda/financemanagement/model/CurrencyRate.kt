package cz.cvut.fel.pda.financemanagement.model

data class CurrencyRate (
    var currencyName: String,
    var rate: Double
)