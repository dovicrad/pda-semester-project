package cz.cvut.fel.pda.financemanagement.ui.utils

import androidx.compose.ui.graphics.Color
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import java.math.BigDecimal

object ColorUtils {
    private val negativeValue = Color(255, 0, 0)
    private val positiveValue = Color(50, 168, 82)
    fun getCategoryColor(category: Category): Color {
        return if (category.type == CategoryType.INCOME) {
            positiveValue
        } else {
            negativeValue
        }
    }
    fun getTotalValueColor(value: BigDecimal): Color {
        return if (value >= BigDecimal.ZERO) {
            positiveValue
        } else {
            negativeValue
        }
    }
}