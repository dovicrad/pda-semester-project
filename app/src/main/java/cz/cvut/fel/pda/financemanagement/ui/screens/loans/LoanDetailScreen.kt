package cz.cvut.fel.pda.financemanagement.ui.screens.loans

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.FinanceManagementScreens
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.database.operation.loan.Loan
import cz.cvut.fel.pda.financemanagement.ui.components.BottomNavigation
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.components.TopBar
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

@Composable
fun LoanDetailScreen(
    navController: NavHostController,
    loanId: Long?,
    loanViewModel: LoanDetailViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    )
){
    loanViewModel.setLoanId(loanId ?: 0L)
    val transactionUiState: LoanUiState by loanViewModel.uiState.collectAsStateWithLifecycle()

    Scaffold (
        topBar = { TopBar(navController) },
        content = { innerPadding ->
            when (transactionUiState) {
                is LoanUiState.Loading -> {
                    ShowLoadingScreen()
                }
                is LoanUiState.Error -> {
                    ShowErrorScreen(
                        errorMessage = (transactionUiState as LoanUiState.Error).exception.message ?: "Error",
                    )
                }
                is LoanUiState.Success -> {
                    LoanScreenContent( (transactionUiState as LoanUiState.Success).loan, innerPadding)
                }
            }
        },
        floatingActionButton = {
            FloatingActionButton(onClick = {
                navController.navigate("${FinanceManagementScreens.EditLoans.name}/${loanId}")
            }) {
                Icon(Icons.Default.Edit, contentDescription = "Edit Loan")
            }
        },
        bottomBar = {
            BottomNavigation(navController = navController)
        }
    )
}

@Composable
fun LoanScreenContent(loan: Loan, innerPadding: PaddingValues) {
    val spacer = Modifier.padding(16.dp, 4.dp)
    Column(modifier = Modifier.padding(innerPadding)) {
        Text(text = "Loan Details", modifier = spacer, style= androidx.compose.ui.text.TextStyle(
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold
        )
        )
        Spacer(modifier = Modifier.height(16.dp))
        Row(modifier = spacer) {
            Text(text = loan.recipient, style = androidx.compose.ui.text.TextStyle(
                    fontSize = 20.sp,
                fontWeight = FontWeight.Bold
            ))
        }
        Row(modifier = spacer) {
            Text(text = "Created: ")
            Text(text = loan.dateCreated.format(
                DateTimeFormatter.ofLocalizedDate(
                FormatStyle.MEDIUM)))
        }
        val textColor = if (loan.type == CategoryType.EXPENSE) Color.Red else Color.Green

        Row(modifier = spacer.fillMaxWidth(), horizontalArrangement = Arrangement.End){
            Text(text = "${loan.amount} ${loan.currency.name}"
                , color = textColor
                , style = androidx.compose.ui.text.TextStyle(
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
        Column(modifier = spacer.fillMaxWidth()){
            Text("Description: ", style = androidx.compose.ui.text.TextStyle(
                fontWeight = FontWeight.Bold
            ))
            Text(loan.description)
        }
    }
}

