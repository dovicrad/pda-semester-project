package cz.cvut.fel.pda.financemanagement.database.operation.transaction

import android.net.Uri
import androidx.room.Entity
import androidx.room.PrimaryKey
import cz.cvut.fel.pda.financemanagement.database.operation.Operation
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import java.math.BigDecimal
import java.time.LocalDateTime


@Entity(tableName = "transaction")
data class Transaction(
    @PrimaryKey(autoGenerate = true)
    override var id: Long = 0,
    override var description: String = "",
    override var amount: BigDecimal = BigDecimal.ZERO,
    override var dateCreated: LocalDateTime = LocalDateTime.now(),
    override var currency: OperationCurrency = OperationCurrency.CZK,
    var imageUrl: Uri = Uri.EMPTY,
    var categoryId: Long,
    var accountId: Long
) : Operation()
