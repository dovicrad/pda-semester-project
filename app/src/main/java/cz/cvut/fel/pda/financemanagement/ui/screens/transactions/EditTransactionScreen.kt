package cz.cvut.fel.pda.financemanagement.ui.screens.transactions

import android.net.Uri
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.outlined.Close
import androidx.compose.material3.Button
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import cz.cvut.fel.pda.financemanagement.R
import cz.cvut.fel.pda.financemanagement.database.account.Account
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.model.TransactionWithCategory
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.accounts.AccountsUiState
import cz.cvut.fel.pda.financemanagement.ui.screens.accounts.AccountsViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.category.CategoriesUiState
import cz.cvut.fel.pda.financemanagement.ui.screens.category.CategoryViewModel
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider
import java.io.File
import java.math.BigDecimal
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@Composable
fun EditTransactionScreen(
    navController: NavHostController,
    transactionId: Long?,
    transactionDetailViewModel: TransactionDetailViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    ),
    categoryViewModel: CategoryViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    ),
    accountsViewModel: AccountsViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    )
) {
    val uiState by transactionDetailViewModel.uiState.collectAsStateWithLifecycle()
    val categoryUiState by categoryViewModel.categories.collectAsStateWithLifecycle()
    val accountsUiState by accountsViewModel.accounts.collectAsStateWithLifecycle()

    when (uiState) {
        is TransactionWithCategoryUiState.Error -> {
            ShowErrorScreen(
                errorMessage = (uiState as TransactionWithCategoryUiState.Error).exception.message ?: "Error",
                onClick = { navController.popBackStack() }
            )
        }
        is TransactionWithCategoryUiState.Loading -> {
            ShowLoadingScreen()
        }
        is TransactionWithCategoryUiState.Success -> {
            when (val categoryState = categoryUiState) {
                is CategoriesUiState.Success -> {
                    when (val accountsState = accountsUiState) {
                        is AccountsUiState.Success -> {
                            ShowSuccessScreen(
                                (uiState as TransactionWithCategoryUiState.Success).transaction,
                                categoryState.itemList,
                                accountsState.itemList,
                                navController,
                                transactionDetailViewModel,
                            )
                        }
                        is AccountsUiState.Loading -> {
                            ShowLoadingScreen()
                        }
                        is AccountsUiState.Error -> {
                            ShowErrorScreen(
                                errorMessage = accountsState.exception.message ?: "Error",
                                onClick = { navController.popBackStack() }
                            )
                        }
                    }
                }
                is CategoriesUiState.Loading -> {
                    ShowLoadingScreen()
                }
                is CategoriesUiState.Error -> {
                    ShowErrorScreen(
                        errorMessage = (categoryUiState as CategoriesUiState.Error).exception.message ?: "Error",
                        onClick = { navController.popBackStack() }
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ShowSuccessScreen(
    transaction: TransactionWithCategory,
    categories: List<Category>,
    accounts: List<Account>,
    navController: NavHostController,
    transactionViewModel: TransactionDetailViewModel,
) {
    val amount = rememberSaveable { mutableStateOf(transaction.transaction.amount.toString()) }
    val currency = rememberSaveable { mutableStateOf(transaction.transaction.currency) }
    val selectedCategory = remember { mutableStateOf(categories.find { it.id == transaction.category.id } ?: categories.getOrNull(0)) }
    val selectedAccount = remember { mutableStateOf(accounts.find { it.id == transaction.transaction.accountId } ?: accounts.getOrNull(0)) }
    val imageUri = rememberSaveable { mutableStateOf(transaction.transaction.imageUrl) }
    val showCameraView = remember { mutableStateOf(false) }
    val cameraExecutor = remember { mutableStateOf<ExecutorService?>(null) }

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text("Transaction edit") },
                colors = TopAppBarDefaults.mediumTopAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primaryContainer
                ),
                modifier = Modifier,
                navigationIcon = {
                    IconButton(onClick = { navController.popBackStack() }) {
                        Icon(
                            imageVector = Icons.Outlined.Close,
                            contentDescription = "Close"
                        )
                    }
                },
                actions = {
                    TextButton(
                        onClick = {
                            transactionViewModel.saveTransaction(
                                amount = BigDecimal(amount.value),
                                category = selectedCategory.value ?: return@TextButton,
                                currency = currency.value,
                                accountId = selectedAccount.value?.id ?: return@TextButton,
                                imageUri = imageUri.value
                            )
                            navController.popBackStack()
                        },
                        modifier = Modifier.padding(16.dp)
                    ) {
                        Text(text = "Save")
                    }
                }
            )
        },
        content = { innerPadding ->
            EditTransactionDetailSuccessContent(
                innerPadding,
                amount,
                currency,
                categories,
                selectedCategory,
                accounts,
                selectedAccount,
                showCameraView,
                cameraExecutor,
                imageUri
            )
        }
    )
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalGlideComposeApi::class)
@Composable
fun EditTransactionDetailSuccessContent(
    innerPadding: PaddingValues,
    amount: MutableState<String>,
    currency: MutableState<OperationCurrency>,
    categories: List<Category>,
    selectedCategory: MutableState<Category?>,
    accounts: List<Account>,
    selectedAccount: MutableState<Account?>,
    showCameraView: MutableState<Boolean>,
    cameraExecutor: MutableState<ExecutorService?>,
    imageUri: MutableState<Uri>
) {
    val showDropdown = remember { mutableStateOf(false) }
    val showCurrencyDropdown = remember { mutableStateOf(false) }
    val showAccountDropdown = remember { mutableStateOf(false) }

    if (showCameraView.value) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            ShowCameraView(cameraExecutor, showCameraView, imageUri)
        }
    } else {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .verticalScroll(rememberScrollState())
                .padding(innerPadding),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            val spaceModifier = Modifier
                .fillMaxWidth()
                .padding(24.dp, 4.dp)

            Box(contentAlignment = Alignment.TopEnd) {
                OutlinedTextField(
                    value = amount.value,
                    onValueChange = { newValue ->
                        if (newValue.all { it.isDigit() }) {
                            amount.value = newValue
                        }
                    },
                    label = { Text("Amount") },
                    trailingIcon = {
                        IconButton(onClick = { showCurrencyDropdown.value = true }) {
                            Text(currency.value.toString())
                        }
                    },
                    modifier = spaceModifier
                )
                DropdownMenu(
                    modifier = spaceModifier,
                    expanded = showCurrencyDropdown.value,
                    onDismissRequest = { showCurrencyDropdown.value = false }
                ) {
                    OperationCurrency.entries.forEachIndexed { index, operationCurrency ->
                        DropdownMenuItem(onClick = {
                            currency.value = operationCurrency
                            showCurrencyDropdown.value = false
                        }, text = {
                            Text(operationCurrency.toString())
                        })
                        if (index < OperationCurrency.entries.size - 1) {
                            HorizontalDivider()
                        }
                    }
                }
            }

            OutlinedTextField(
                value = selectedCategory.value?.name.orEmpty(),
                onValueChange = {},
                label = { Text("Category") },
                trailingIcon = {
                    IconButton(onClick = { showDropdown.value = true }) {
                        Icon(Icons.Filled.ArrowDropDown, contentDescription = null)
                    }
                },
                readOnly = true,
                modifier = spaceModifier
            )
            DropdownMenu(
                expanded = showDropdown.value,
                onDismissRequest = { showDropdown.value = false }
            ) {
                categories.forEach { category ->
                    DropdownMenuItem(onClick = {
                        selectedCategory.value = category
                        showDropdown.value = false
                    }, text = {
                        Text(category.name)
                    })
                }
            }

            OutlinedTextField(
                value = selectedAccount.value?.name.orEmpty(),
                onValueChange = {},
                label = { Text("Account") },
                trailingIcon = {
                    IconButton(onClick = { showAccountDropdown.value = true }) {
                        Icon(Icons.Filled.ArrowDropDown, contentDescription = null)
                    }
                },
                readOnly = true,
                modifier = spaceModifier
            )
            DropdownMenu(
                expanded = showAccountDropdown.value,
                onDismissRequest = { showAccountDropdown.value = false }
            ) {
                accounts.forEach { account ->
                    DropdownMenuItem(onClick = {
                        selectedAccount.value = account
                        showAccountDropdown.value = false
                    }, text = {
                        Text(account.name)
                    })
                }
            }

            Button(onClick = {
                cameraExecutor.value = Executors.newSingleThreadExecutor()
                showCameraView.value = true
            }) {
                Text(text = "Take Picture")
            }

            if (imageUri.value != Uri.EMPTY) {
                GlideImage(
                    model = imageUri.value,
                    contentDescription = "Captured Image",
                    contentScale = ContentScale.Inside,
                    modifier = Modifier
                        .size(200.dp)
                        .fillMaxSize(0.5f)
                )
            }
        }
    }
}

@Composable
fun ShowCameraView(
    cameraExecutor: MutableState<ExecutorService?>,
    showCameraView: MutableState<Boolean>,
    imageUri: MutableState<Uri>
) {
    fun handleImageCapture(uri: Uri) {
        imageUri.value = uri
        showCameraView.value = false
    }

    CameraView(
        outputDirectory = getOutputDirectory(),
        executor = cameraExecutor.value!!,
        onImageCaptured = ::handleImageCapture,
        onError = { /* Handle error */ }
    )
}

@Composable
private fun getOutputDirectory(): File {
    val context = LocalContext.current
    val mediaDir = context.getExternalFilesDirs(null).firstOrNull()?.let {
        File(it, context.resources.getString(R.string.app_name)).apply { mkdirs() }
    }
    return if (mediaDir != null && mediaDir.exists()) mediaDir else context.filesDir
}
