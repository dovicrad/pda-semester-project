package cz.cvut.fel.pda.financemanagement.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.FinanceManagementScreens

@Composable
fun BottomNavigation(navController: NavHostController) {
    val items = listOf(
        Pair("Transactions", FinanceManagementScreens.Transactions.name),
        Pair("Accounts", FinanceManagementScreens.Accounts.name),
        Pair( "Loans", FinanceManagementScreens.Loans.name),
        Pair( "Categories", FinanceManagementScreens.Categories.name),
    )

    NavigationBar {
        val currentRoute = navController.currentDestination?.route

        items.forEach {
            NavigationBarItem(
                icon = {  GreyDot() },
                label = { Text(it.first) },
                selected = currentRoute == it.second,
                onClick = {
                    navController.navigate(it.second) {
                        launchSingleTop = true
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        restoreState = true
                    }
                }
            )
        }
    }
}

@Composable
fun GreyDot() {
    Box(modifier = Modifier
            .size(16.dp)
            .background(Color.DarkGray, CircleShape)
    )
}