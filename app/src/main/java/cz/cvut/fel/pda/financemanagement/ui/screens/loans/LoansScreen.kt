package cz.cvut.fel.pda.financemanagement.ui.screens.loans

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.outlined.Add
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.FinanceManagementScreens
import cz.cvut.fel.pda.financemanagement.database.operation.loan.Loan
import cz.cvut.fel.pda.financemanagement.ui.components.BottomNavigation
import cz.cvut.fel.pda.financemanagement.ui.components.CreateDrawerContent
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.transactions.CreateTopBar
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider

@Composable
fun LoansScreen(
    navController: NavHostController,
    loanViewModel: LoanViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    )
){
    val loansUiState: LoansUiState by loanViewModel.loans.collectAsStateWithLifecycle()

    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()

    ModalNavigationDrawer(drawerContent = { CreateDrawerContent(navController, drawerState, scope) }, drawerState = drawerState) {
        Scaffold(
            topBar = { CreateTopBar(scope, drawerState) },
            content = { innerPadding -> LoansScreenContent(navController, innerPadding, loansUiState) },
            floatingActionButton = { FloatingActionButton(onClick = { navController.navigate("${FinanceManagementScreens.EditLoans.name}/0") }) {
                Icon(Icons.Outlined.Add, contentDescription = "Add loan")
            } },
            bottomBar = { BottomNavigation(navController = navController) },
        )
    }
}

@Composable
fun LoansScreenContent(
    navController: NavHostController,
    innerPadding: PaddingValues,
    loansUiState: LoansUiState
) {
    when (loansUiState) {
        is LoansUiState.Loading -> ShowLoadingScreen()
        is LoansUiState.Error -> ShowErrorScreen(
            errorMessage = loansUiState.exception.message ?: "Error",
        )
        is LoansUiState.Success -> {
            ShowSuccessLoanScreen(
                loansUiState,
                navController,
                innerPadding
            )
        }
    }
}

@Composable
fun LoanItem(loan: Loan, navController: NavHostController) {
    var showMenu by remember { mutableStateOf(false) }
    Row(
        modifier = Modifier
            .fillMaxWidth(0.8f)
            .padding(vertical = 8.dp)
            .border(1.dp, Color.Gray, shape = RoundedCornerShape(3.dp))
            .clickable { navController.navigate("${FinanceManagementScreens.LoansDetail.name}/${loan.id}") }
            .padding(8.dp)
        , horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column {
            Text(text = loan.recipient, style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold))
            Text(
                text = loan.amount.toString()+" "+loan.currency.name,
                style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold)
            ) }
        Box {
            IconButton(onClick = { showMenu = true }) {
                Icon(Icons.Filled.MoreVert, contentDescription = "Options")
            }
            DropdownMenu(
                expanded = showMenu,
                onDismissRequest = { showMenu = false }
            ) {
                DropdownMenuItem(onClick = {
                    navController.navigate("${FinanceManagementScreens.EditLoans.name}/${loan.id}")
                    showMenu = false
                }, text = { Text("Edit") }
                )
                DropdownMenuItem(onClick = {
                    // Handle delete loan here
                    // loanViewModel.deleteLoan(loan.id)
                    showMenu = false
                }, text = { Text("Delete")})
            }
        }
    }
}

@Composable
fun ShowSuccessLoanScreen(loansUiState: LoansUiState.Success, navController: NavHostController, innerPadding: PaddingValues) {
    val loanList = loansUiState.itemList
    if (loanList.isEmpty()){
        Box (
            modifier = Modifier.fillMaxSize().padding(innerPadding),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = "No loans found.",
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(16.dp)
            )
        }
    } else {
        LazyColumn(
            modifier = Modifier.fillMaxSize().padding(innerPadding),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            items(loanList) {loan ->
                LoanItem(loan, navController)
            }
        }
    }
}