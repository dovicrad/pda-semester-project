package cz.cvut.fel.pda.financemanagement.database.account

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.Transaction
import kotlinx.coroutines.flow.Flow
import java.math.BigDecimal

@Dao
interface AccountDao {

    @Query("SELECT * FROM `account` ORDER BY id ASC")
    fun getAllAccounts(): Flow<List<Account>>

    @Query("SELECT * FROM `account` WHERE id = :id")
    fun getAccountForId(id: Long): Flow<Account?>

    @Query("""
        SELECT t.*, c.*
        FROM `transaction` t
        INNER JOIN `category` c ON t.categoryId = c.id
        WHERE t.accountId = :accountId
    """)
    fun getTransactionsWithCategoryForAccount(accountId: Long): Flow<Map<Transaction, Category>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAccounts(vararg accounts: Account)

    @Update
    suspend fun updateAccounts(vararg accounts: Account)

    @Delete
    suspend fun delete(item: Account)

    @Query("DELETE FROM `account`")
    suspend fun deleteAll()

    @Query("DELETE FROM `transaction` WHERE accountId = :accountId")
    suspend fun deleteTransactionsByAccountId(accountId: Long)

    @Query("UPDATE `account` SET balance = balance + :amount WHERE id = :accountId")
    suspend fun updateAccountBalance(accountId: Long, amount: BigDecimal)
}
