package cz.cvut.fel.pda.financemanagement.model

import androidx.room.Embedded
import androidx.room.Relation
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.Transaction

data class TransactionWithCategory(
    @Embedded val transaction: Transaction,
    @Relation(
        parentColumn = "categoryId",
        entityColumn = "id"
    )
    val category: Category,
)