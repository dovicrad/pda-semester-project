package cz.cvut.fel.pda.financemanagement.database.operation.loan

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface LoanDao {

    @Query("SELECT * FROM `loan` ORDER BY id ASC")
    fun getAllLoans(): Flow<List<Loan>>

    @Query("SELECT * FROM `loan` WHERE id = :id")
    fun getLoanForId(id: Long): Flow<Loan>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLoans(vararg loans: Loan)

    @Update
    suspend fun updateLoans(vararg loans: Loan)

    @Delete
    suspend fun delete(item: Loan)

    @Query("DELETE FROM `loan`")
    suspend fun deleteAll()
}