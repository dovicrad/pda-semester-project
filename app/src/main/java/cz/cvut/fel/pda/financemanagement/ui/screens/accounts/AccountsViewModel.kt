package cz.cvut.fel.pda.financemanagement.ui.screens.accounts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fel.pda.financemanagement.database.account.Account
import cz.cvut.fel.pda.financemanagement.database.account.AccountRepository
import cz.cvut.fel.pda.financemanagement.datastore.PreferencesRepository
import cz.cvut.fel.pda.financemanagement.model.common.Result
import cz.cvut.fel.pda.financemanagement.model.common.asResult
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class AccountsViewModel(
    private val accountRepository: AccountRepository,
    private val preferencesRepository: PreferencesRepository
) : ViewModel() {
    val accounts: StateFlow<AccountsUiState> =
        accountRepository
            .getAllAccountsStream()
            .asResult()
            .map { allAccountsToResult ->
                when (allAccountsToResult) {
                    is Result.Error -> AccountsUiState.Error(Exception(allAccountsToResult.exception))
                    is Result.Loading -> AccountsUiState.Loading
                    is Result.Success -> AccountsUiState.Success(allAccountsToResult.data)
                }
            }.stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5000L),
                initialValue = AccountsUiState.Loading
            )

    fun deleteAccountWithTransactions(account: Account) {
        viewModelScope.launch {
            accountRepository.deleteAccountWithTransactions(account)
        }
    }
}

sealed interface AccountsUiState {
    data object Loading : AccountsUiState
    data class Success(val itemList: List<Account> = listOf()) : AccountsUiState
    data class Error(val exception: Exception) : AccountsUiState
}
