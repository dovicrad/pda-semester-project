package cz.cvut.fel.pda.financemanagement.ui.utils

import android.Manifest
import android.app.Notification
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import cz.cvut.fel.pda.financemanagement.R

class LoanNotificationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val notification = createNotification(context)
        displayNotification(context, notification)
    }

    private fun createNotification(context: Context): Notification {
        val builder = NotificationCompat.Builder(context, "cz.cvut.fel.pda.financemanagement")
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentTitle("Unreturned Loans")
            .setContentText("You have some unreturned loans.")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        return builder.build()
    }

    private fun displayNotification(context: Context, notification: Notification) {
        val notificationManager = NotificationManagerCompat.from(context)
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.POST_NOTIFICATIONS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        notificationManager.notify(NOTIFICATION_ID, notification)

    }

    companion object {
        private const val NOTIFICATION_ID = 1 // Define your notification ID here
    }
}