package cz.cvut.fel.pda.financemanagement.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import cz.cvut.fel.pda.financemanagement.database.account.Account
import cz.cvut.fel.pda.financemanagement.database.account.AccountDao
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.category.CategoryDao
import cz.cvut.fel.pda.financemanagement.database.operation.loan.Loan
import cz.cvut.fel.pda.financemanagement.database.operation.loan.LoanDao
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.Transaction
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.TransactionDao
import cz.cvut.fel.pda.financemanagement.database.typeConvertion.Converters

@Database(entities = [Transaction::class, Account::class, Loan::class, Category::class], version = 11, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase(){
    abstract fun transactionDao(): TransactionDao
    abstract fun categoryDao(): CategoryDao
    abstract fun loanDao(): LoanDao
    abstract fun accountDao(): AccountDao
    companion object{
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "database"
                )
                    .fallbackToDestructiveMigration()
                    .build()
                    .also {
                        INSTANCE = it
                    }
            }
        }
    }
}