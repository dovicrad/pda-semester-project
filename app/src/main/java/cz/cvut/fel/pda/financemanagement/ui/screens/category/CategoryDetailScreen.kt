package cz.cvut.fel.pda.financemanagement.ui.screens.category

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.FinanceManagementScreens
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.datastore.PreferencesRepository
import cz.cvut.fel.pda.financemanagement.model.CategoryWithTransactions
import cz.cvut.fel.pda.financemanagement.ui.components.BottomNavigation
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.components.TopBar
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider
import cz.cvut.fel.pda.financemanagement.ui.utils.CurrencyExchangeProvider
import kotlinx.coroutines.runBlocking
import java.math.BigDecimal
import java.math.RoundingMode


@Composable
fun CategoryDetailScreen(
    navController: NavHostController,
    categoryId: Long?,
    categoryViewModel: CategoryDetailViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    )
){
    categoryViewModel.setCategoryId(categoryId ?: 0L)
    val  categoryDetailUiState:  CategoryDetailUiState by categoryViewModel.uiState.collectAsStateWithLifecycle()

    Scaffold (
        topBar = {TopBar(navController)},
        content = {
            innerPadding ->
            when (categoryDetailUiState) {
                is  CategoryDetailUiState.Loading -> {
                    ShowLoadingScreen()
                }
                is  CategoryDetailUiState.Error -> {
                    ShowErrorScreen(
                        errorMessage = (categoryDetailUiState as  CategoryDetailUiState.Error).exception.message ?: "Error",
                    )
                }
                is  CategoryDetailUiState.Success -> {
                    CategoryScreenContent( (categoryDetailUiState as  CategoryDetailUiState.Success).category, innerPadding, navController)
                }
            }
        },
        floatingActionButton = {
            FloatingActionButton(onClick = {
                navController.navigate("${FinanceManagementScreens.EditCategory.name}/${categoryId}")
            }) {
                Icon(Icons.Default.Edit, contentDescription = "Edit Category")
            }
        },
        bottomBar = {
            BottomNavigation(navController = navController)
        }
    )
}

@Composable
fun CategoryScreenContent(category: CategoryWithTransactions, innerPadding: PaddingValues, navController: NavHostController) {
    val repo = PreferencesRepository(LocalContext.current)
    val myCurrency = runBlocking {   repo.getUserPreferences().preferredCurrency}
    val totalSum = category.transaction.sumOf { it.amount * BigDecimal(
        CurrencyExchangeProvider.getExchangeRate(it.currency.name, repo)) }
    val flooredTotalSum = totalSum.setScale(0, RoundingMode.FLOOR)

    val spacer = Modifier.padding(16.dp, 4.dp)

    Column(modifier = Modifier.padding(innerPadding)) {
        Text(text = "Category Details: ", modifier = spacer, style= androidx.compose.ui.text.TextStyle(
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold
        ))
        Spacer(modifier = Modifier.height(16.dp))
        Row(modifier = spacer) {
            Text(text = category.category.name, style = androidx.compose.ui.text.TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold
            ))
        }
        Row(modifier = spacer) {
            Text(text = category.category.type.toString(), style = androidx.compose.ui.text.TextStyle(
                fontSize = 12.sp,
                fontWeight = FontWeight.Bold
            ))
        }
        val textColor = if (category.category.type == CategoryType.EXPENSE) Color.Red else Color.Green

        Row(modifier = spacer.fillMaxWidth(), horizontalArrangement = Arrangement.End){
            Text(text = "$flooredTotalSum $myCurrency"
                , color = textColor
                , style = androidx.compose.ui.text.TextStyle(
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }

    }
}