package cz.cvut.fel.pda.financemanagement.ui.screens.transactions

import android.net.Uri
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fel.pda.financemanagement.database.account.AccountRepository
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.Transaction
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.TransactionRepository
import cz.cvut.fel.pda.financemanagement.model.TransactionWithCategory
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.math.BigDecimal

class TransactionDetailViewModel(
    private val transactionRepository: TransactionRepository,
    private val accountRepository: AccountRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    private var transactionId: Long = savedStateHandle.get<Long>("Id")!!

    val uiState: StateFlow<TransactionWithCategoryUiState> =
        transactionRepository
            .getTransactionWithCategoryForIdStream(transactionId)
            .map {
                if (it != null) {
                    TransactionWithCategoryUiState.Success(it)
                } else if (transactionId == 0L) {
                    TransactionWithCategoryUiState.Success(
                        TransactionWithCategory(
                            transaction = Transaction(categoryId = 0L, accountId = 0L),
                            category = Category(0, "Groceries", CategoryType.INCOME)
                        )
                    )
                } else {
                    TransactionWithCategoryUiState.Error(Exception("Transaction not found"))
                }
            }.stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5000L),
                initialValue = TransactionWithCategoryUiState.Loading
            )

    fun setTransactionId(id: Long) {
        transactionId = id
    }

    fun saveTransaction(
        amount: BigDecimal,
        category: Category,
        currency: OperationCurrency,
        accountId: Long,
        imageUri: Uri
    ) {
        viewModelScope.launch {
            val transaction = Transaction(
                id = transactionId,
                amount = amount,
                categoryId = category.id,
                currency = currency,
                accountId = accountId,
                imageUrl = imageUri
            )

            if (transactionId == 0L) {
                transactionRepository.insertTransactions(transaction)
                updateAccountBalance(accountId, amount, category.type)
            } else {
                // Get the old transaction details
                val oldTransaction = transactionRepository.getTransactionForIdStream(transactionId).firstOrNull()
                if (oldTransaction != null) {
                    transactionRepository.updateTransactions(transaction)
                    // Update account balance based on the difference
                    val difference = amount.subtract(oldTransaction.amount)
                    updateAccountBalance(accountId, difference, category.type)
                }
            }
        }
    }

    private suspend fun updateAccountBalance(accountId: Long, amount: BigDecimal, categoryType: CategoryType) {
        val adjustment = if (categoryType == CategoryType.INCOME) {
            amount
        } else {
            amount.negate()
        }
        accountRepository.updateAccountBalance(accountId, adjustment)
    }
}

sealed interface TransactionWithCategoryUiState {
    data object Loading : TransactionWithCategoryUiState
    data class Success(val transaction: TransactionWithCategory) : TransactionWithCategoryUiState
    data class Error(val exception: Exception) : TransactionWithCategoryUiState
}
