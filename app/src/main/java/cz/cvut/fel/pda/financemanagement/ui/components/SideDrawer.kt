package cz.cvut.fel.pda.financemanagement.ui.components

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Build
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Notifications
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.DrawerState
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.FinanceManagementScreens
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun CreateDrawerContent(
    navController: NavHostController,
    drawerState: DrawerState,
    scope: CoroutineScope
) {
    val context = LocalContext.current
    val openURL = Intent(Intent.ACTION_VIEW)
    openURL.data = Uri.parse("https://docs.google.com/document/d/1RiUxv3JqMAsXMej93GppAsk0SKVlIzv-TOwqTlRupb0/edit")

    ModalDrawerSheet(modifier = Modifier.fillMaxWidth(0.75f)){
        CreateDrawerItem(Icons.Filled.Close, "Close") {scope.launch {drawerState.close()}  }
        CreateDrawerItem(Icons.Filled.Settings, "Preferences") { navController.navigate(
            FinanceManagementScreens.Preferences.name) }
        HorizontalDivider()
        CreateDrawerItem(Icons.Filled.Build, "Docs") { context.startActivity(openURL) }
        HorizontalDivider()
        CreateDrawerItem(Icons.Filled.Notifications, "Notifications") { navController.navigate(
            FinanceManagementScreens.Notifications.name
        ) }
    }
}

@Composable
fun CreateDrawerItem(icon: ImageVector, text: String, function: () -> Unit) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .clickable { function() }
            .fillMaxWidth()
            .padding(16.dp)
    ) {
        Icon(
            icon,
            contentDescription = "$text Icon",
            modifier = Modifier.size(24.dp)
        )
        Spacer(modifier = Modifier.width(16.dp))
        Text(
            text = text,
            style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold)
        )
    }
}