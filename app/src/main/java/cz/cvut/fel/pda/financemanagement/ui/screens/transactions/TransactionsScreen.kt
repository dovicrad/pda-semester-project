package cz.cvut.fel.pda.financemanagement.ui.screens.transactions

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.outlined.Add
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.FinanceManagementScreens
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.Transaction
import cz.cvut.fel.pda.financemanagement.datastore.PreferencesRepository
import cz.cvut.fel.pda.financemanagement.model.TransactionWithCategory
import cz.cvut.fel.pda.financemanagement.ui.components.BottomNavigation
import cz.cvut.fel.pda.financemanagement.ui.components.CreateDrawerContent
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.preferences.PreferencesUiState
import cz.cvut.fel.pda.financemanagement.ui.screens.preferences.PreferencesViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.preferences.PreferencesViewModelFactory
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider
import cz.cvut.fel.pda.financemanagement.ui.utils.ColorUtils.getCategoryColor
import cz.cvut.fel.pda.financemanagement.ui.utils.ColorUtils.getTotalValueColor
import cz.cvut.fel.pda.financemanagement.ui.utils.CurrencyExchangeProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.math.RoundingMode

@Composable
fun TransactionsScreen(
    navController: NavHostController,
    transactionViewModel: TransactionViewModel= viewModel(
        factory = AppViewModelProvider.Factory
    ),
    preferencesViewModel: PreferencesViewModel = viewModel(
        factory = PreferencesViewModelFactory(
            PreferencesRepository(LocalContext.current)
        )
    )
) {
    val transactionsUiState: TransactionsWithCategoryUiState by transactionViewModel.transactionsWithCategory.collectAsStateWithLifecycle()
    val preferencesUiState: PreferencesUiState by preferencesViewModel.preferencesUiStateFlow.collectAsStateWithLifecycle()

    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()

    ModalNavigationDrawer(drawerContent = { CreateDrawerContent(navController, drawerState, scope) }, drawerState = drawerState) {
        Scaffold(
            topBar = { CreateTopBar(scope, drawerState) },
            content = { innerPadding -> CreateContent(transactionsUiState,preferencesUiState, navController, innerPadding) },
            floatingActionButton = { CreateFloatingActionButton(navController) },
            bottomBar = { BottomNavigation(navController = navController) },
        )
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CreateTopBar(scope: CoroutineScope, drawerState: DrawerState) {
    TopAppBar(title = { Text(text = "CoinKeeper") },
        navigationIcon = {
            IconButton(onClick = {
                scope.launch {
                    drawerState.open()
                }
            }, content = {
                Icon(Icons.Filled.Menu, contentDescription = "Menu")
            })
        })
}

@Composable
fun CreateContent(
    transactionsUiState: TransactionsWithCategoryUiState,
    preferencesUiState: PreferencesUiState,
    navController: NavHostController,
    innerPadding: PaddingValues) {
    when (transactionsUiState) {
        is TransactionsWithCategoryUiState.Loading -> ShowLoadingScreen()
        is TransactionsWithCategoryUiState.Error -> ShowErrorScreen(
            errorMessage = transactionsUiState.exception.message ?: "Error",
        )
        is TransactionsWithCategoryUiState.Success -> {
            when (preferencesUiState) {
                is PreferencesUiState.Loading -> ShowLoadingScreen()
                is PreferencesUiState.Error -> ShowErrorScreen(
                    errorMessage = preferencesUiState.exception.message ?: "Error",
                )
                is PreferencesUiState.Success -> {
                    val preferredCurrency = preferencesUiState.preferences.preferredCurrency
                    ShowSuccessScreen(
                        transactionsUiState,
                        preferredCurrency,
                        navController,
                        innerPadding
                    )
                }
            }
        }
    }
}
@Composable
fun ShowSuccessScreen(
    transactionsUiState: TransactionsWithCategoryUiState.Success,
    preferredCurrency: OperationCurrency,
    navController: NavHostController,
    innerPadding: PaddingValues
) {
    TransactionScreenContent(
        transactionsUiState.itemList,
        preferredCurrency,
        navController,
        innerPadding
    )
}

@Composable
fun CreateFloatingActionButton(navController: NavHostController) {
    FloatingActionButton(onClick = { navController.navigate("${FinanceManagementScreens.EditTransaction.name}/0") }) {
        Icon(Icons.Outlined.Add, contentDescription = "Add transaction")
    }
}

@Composable
fun TransactionScreenContent(
    transactions: List<TransactionWithCategory>,
    preferredCurrency: OperationCurrency,
    navController: NavHostController,
    innerPadding: PaddingValues
) {
    val sortState = remember { mutableStateOf("Don't sort") }
    val sortedTransactions = when (sortState.value) {
        "Sort by Amount Asc." -> transactions.sortedBy { it.transaction.amount }
        "Sort by Date Asc." -> transactions.sortedBy { it.transaction.dateCreated }
        "Sort by Amount Desc." -> transactions.sortedByDescending { it.transaction.amount }
        "Sort by Date Desc." -> transactions.sortedByDescending { it.transaction.dateCreated }
        else -> transactions
    }

    Column(modifier = Modifier
        .padding(innerPadding)
        .fillMaxWidth()) {
        TotalSumOfTransactions(sortedTransactions, preferredCurrency)
        Column(modifier = Modifier.padding(horizontal = 14.dp)) {
            Row(modifier = Modifier
                .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ){
                Text(text = "Transactions", style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold))
                SortDropdown(sortState)
            }
            HorizontalDivider()
            ShowTabListSuccessScreen(sortedTransactions, navController)
        }
    }
}

@Composable
fun SortDropdown(sortState: MutableState<String>) {
    var expanded by remember { mutableStateOf(false) }
    val options = listOf("Don't sort", "Sort by Amount Asc.", "Sort by Date Asc.", "Sort by Amount Desc.", "Sort by Date Desc.")

    Row(verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.clickable { expanded = true }) {
        Box{
            Text(text = sortState.value)
            DropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
                options.forEach { label ->
                    DropdownMenuItem(
                        onClick = {
                            sortState.value = label
                            expanded = false
                        },
                        text = { Text(label) }
                    )
                }
            }
        }
        Icon(Icons.Filled.ArrowDropDown, contentDescription = "Sort options")
    }
}


@Composable
fun TotalSumOfTransactions(transactions: List<TransactionWithCategory>, preferredCurrency: OperationCurrency) {
    val repo = PreferencesRepository(LocalContext.current)
    val totalIncome = transactions.filter { it.category.type == CategoryType.INCOME }.sumOf { it.transaction.amount * BigDecimal(CurrencyExchangeProvider.getExchangeRate(it.transaction.currency.name, repo)) }
    val totalExpense = transactions.filter { it.category.type == CategoryType.EXPENSE }.sumOf { it.transaction.amount * BigDecimal(CurrencyExchangeProvider.getExchangeRate(it.transaction.currency.name, repo)) }

    val totalSum = ((totalExpense.times((-1).toBigDecimal())) + totalIncome)
    val flooredTotalSum = totalSum.setScale(0, RoundingMode.FLOOR)

    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(14.dp),
        horizontalArrangement = Arrangement.Center){
        Text(text = "Total: ", style = TextStyle(fontSize = 24.sp, fontWeight = FontWeight.Bold))
        Text(text = "$flooredTotalSum ${preferredCurrency.name}",
            style = TextStyle(
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
                color = getTotalValueColor(flooredTotalSum)
            )
        )
    }
}

@Composable
fun ShowTabListSuccessScreen(itemList: List<TransactionWithCategory>, navController: NavHostController) {
    if (itemList.isEmpty()){
        Box (
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = "No transactions found.",
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(16.dp)
            )
        }
    } else {
        TransactionsList(itemList, navController)
    }
}

@Composable
fun TransactionsList(
    itemList: List<TransactionWithCategory>,
    navController: NavHostController,
    modifier: Modifier = Modifier
) {
    LazyColumn(
        modifier = modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Top
    ) {
        items(itemList) {transaction ->
            TransactionItem(transaction.transaction, transaction.category, navController)
        }
    }
}

@Composable
fun TransactionItem(transaction: Transaction, category: Category, navController: NavHostController) {
    var showMenu by remember { mutableStateOf(false) }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp)
            .border(1.dp, Color.Gray, shape = RoundedCornerShape(3.dp))
            .clickable { navController.navigate("${FinanceManagementScreens.TransactionsDetail.name}/${transaction.id}") }
            .padding(8.dp)
        , horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column {
            Text(text = category.name, style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold))
            Text(
                text = (if (category.type == CategoryType.EXPENSE) "-${transaction.amount}" else transaction.amount.toString())+" "+transaction.currency.name,
                color = getCategoryColor(category),
                style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold)
            ) }
        Box {
            IconButton(onClick = { showMenu = true }) {
                Icon(Icons.Filled.MoreVert, contentDescription = "Options")
            }
            DropdownMenu(
                expanded = showMenu,
                onDismissRequest = { showMenu = false }
            ) {
                DropdownMenuItem(onClick = {
                    navController.navigate("${FinanceManagementScreens.EditTransaction.name}/${transaction.id}")
                    showMenu = false
                }, text = { Text("Edit") }
                )
                DropdownMenuItem(onClick = {
                    showMenu = false
                }, text = { Text("Delete")})
            }
        }
    }
}