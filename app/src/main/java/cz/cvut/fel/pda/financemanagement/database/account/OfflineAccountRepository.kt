package cz.cvut.fel.pda.financemanagement.database.account

import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.Transaction
import kotlinx.coroutines.flow.Flow
import java.math.BigDecimal

class OfflineAccountRepository(private val accountDao: AccountDao) : AccountRepository {
    override fun getAllAccountsStream(): Flow<List<Account>> = accountDao.getAllAccounts()
    override fun getAccountForIdStream(id: Long): Flow<Account?> = accountDao.getAccountForId(id)
    override suspend fun insertAccounts(vararg accounts: Account) = accountDao.insertAccounts(*accounts)
    override suspend fun deleteAccount(account: Account) = accountDao.delete(account)
    override suspend fun deleteAllAccounts() = accountDao.deleteAll()
    override suspend fun updateAccounts(vararg accounts: Account) = accountDao.updateAccounts(*accounts)
    override fun getTransactionsWithCategoryForAccount(accountId: Long): Flow<Map<Transaction, Category>> = accountDao.getTransactionsWithCategoryForAccount(accountId)
    override suspend fun deleteAccountWithTransactions(account: Account) {
        accountDao.deleteTransactionsByAccountId(account.id)
        accountDao.delete(account)
    }
    override suspend fun updateAccountBalance(accountId: Long, amount: BigDecimal) {
        accountDao.updateAccountBalance(accountId, amount)
    }
}
