package cz.cvut.fel.pda.financemanagement.database.category

import cz.cvut.fel.pda.financemanagement.model.CategoryWithTransactions
import kotlinx.coroutines.flow.Flow

interface CategoryRepository {
    fun getAllCategoriesStream(): Flow<List<Category>>
    fun getCategoryForIdStream(id: Long): Flow<Category?>
    fun getCategoryWithTransactionsForIdStream(id: Long): Flow<CategoryWithTransactions?>
    suspend fun insertCategories(vararg categories: Category)
    suspend fun deleteCategory(category: Category)
    suspend fun deleteAllCategories()
    suspend fun updateCategory(vararg category: Category)
}