package cz.cvut.fel.pda.financemanagement.ui.screens.preferences

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.model.Preferences
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider

@Composable
fun PreferencesScreen(
    navController: NavHostController,
    preferencesViewModel: PreferencesViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    )
    ) {
    val preferencesUiState by preferencesViewModel.preferencesUiStateFlow.collectAsStateWithLifecycle()
    when(val state = preferencesUiState){
        is PreferencesUiState.Success -> {
            ShowPreferencesContentScreen(
                state.preferences,
                navController,
                preferencesViewModel
            )
        }
        PreferencesUiState.Loading -> {
            ShowLoadingScreen()
        }
        is PreferencesUiState.Error -> {
            ShowErrorScreen(
                errorMessage = state.exception.message ?: "Error",
                onClick = { navController.popBackStack() }
            )
        }
    }
}

@Composable
fun ShowPreferencesContentScreen(
    preferences: Preferences,
    navController: NavHostController,
    preferencesViewModel: PreferencesViewModel
) {
    Scaffold(
        topBar = {
            Row(verticalAlignment = Alignment.CenterVertically) {
                IconButton(onClick = { navController.popBackStack()}) {
                    Icon(Icons.AutoMirrored.Filled.ArrowBack, contentDescription = "Back")
                }
                Text("Preferences", style = MaterialTheme.typography.titleLarge)
            }
        },
        content = { innerPadding ->
            Box(contentAlignment = Alignment.TopEnd, modifier = Modifier.padding(innerPadding)) {
                var expanded by rememberSaveable { mutableStateOf(false) }
                val preferredCurrency = rememberSaveable { mutableStateOf(preferences.preferredCurrency) }
                val spaceModifier = Modifier.fillMaxWidth().padding(24.dp, 4.dp)

                OutlinedTextField(
                    value = "Current currency",
                    onValueChange = {},
                    label = { Text("Preferred Currency") },
                    trailingIcon = {
                        IconButton(onClick = { expanded = true }) {
                            Text(preferredCurrency.value.name)
                        }
                    },
                    readOnly = true,
                    modifier = spaceModifier
                )
                DropdownMenu(
                    modifier = spaceModifier,
                    expanded = expanded,
                    onDismissRequest = { expanded = false }
                ) {
                    OperationCurrency.entries.forEachIndexed { index, currency ->
                        DropdownMenuItem(
                            onClick = {
                                preferredCurrency.value = currency
                                preferencesViewModel.saveCurrencyPreference(currency)
                                expanded = false
                            },
                            text = { Text(currency.name) }
                        )
                        if (index < OperationCurrency.entries.size - 1) {
                            HorizontalDivider()
                        }
                    }
                }
            }
        }
    )
}
