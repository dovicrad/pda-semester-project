package cz.cvut.fel.pda.financemanagement.database.category

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import cz.cvut.fel.pda.financemanagement.model.CategoryWithTransactions
import kotlinx.coroutines.flow.Flow

@Dao
interface CategoryDao {

    @Query("SELECT * FROM `category` ORDER BY id ASC")
    fun getAllCategories(): Flow<List<Category>>

    @Query("SELECT * FROM `category` WHERE id = :id")
    fun getCategoryForId(id: Long): Flow<Category>

    @Query("SELECT * FROM `category` JOIN `transaction` ON `category`.id = `transaction`.categoryId  WHERE category.id = :id ")
    fun getCategoryWithTransactionsForId(id: Long): Flow<CategoryWithTransactions>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategories(vararg transactions: Category)

    @Update
    suspend fun updateCategories(vararg transactions: Category)

    @Delete
    suspend fun delete(item: Category)

    @Query("DELETE FROM `category`")
    suspend fun deleteAll()

    @Query("SELECT * FROM `category` ORDER BY id ASC")
    suspend fun getCategories(): List<Category>
}