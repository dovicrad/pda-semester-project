package cz.cvut.fel.pda.financemanagement.ui.screens.category

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.category.CategoryRepository
import cz.cvut.fel.pda.financemanagement.model.common.Result
import cz.cvut.fel.pda.financemanagement.model.common.asResult
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn

class CategoryViewModel(
    categoryRepository: CategoryRepository,
) : ViewModel(){
    val categories : StateFlow<CategoriesUiState> =
        categoryRepository
            .getAllCategoriesStream()
            .asResult()
            .map { allCategoriesToResult ->
                when ( allCategoriesToResult) {
                    is Result.Error -> CategoriesUiState.Error(Exception( allCategoriesToResult.exception))
                    is Result.Loading -> CategoriesUiState.Loading
                    is Result.Success -> CategoriesUiState.Success( allCategoriesToResult.data)
                }
            }.stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5000L),
                initialValue = CategoriesUiState.Loading
            )
}

sealed interface CategoriesUiState {
    data object Loading : CategoriesUiState
    data class Success(val itemList: List<Category> = listOf()) : CategoriesUiState
    data class Error(val exception: Exception) : CategoriesUiState
}