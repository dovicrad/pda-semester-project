package cz.cvut.fel.pda.financemanagement.database.operation

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.math.BigDecimal
import java.time.LocalDateTime

@Entity(tableName = "operation")
abstract class Operation {
        @PrimaryKey(autoGenerate = true)
        open var id: Long = 0
        abstract var description: String
        abstract var amount: BigDecimal
        abstract var dateCreated: LocalDateTime
        abstract var currency: OperationCurrency
}

enum class OperationCurrency {
    CZK,
    EUR,
    USD,
    ISK,
    GBP
}