package cz.cvut.fel.pda.financemanagement.di

import android.content.Context
import cz.cvut.fel.pda.financemanagement.database.AppDatabase
import cz.cvut.fel.pda.financemanagement.database.account.AccountRepository
import cz.cvut.fel.pda.financemanagement.database.account.OfflineAccountRepository
import cz.cvut.fel.pda.financemanagement.database.category.CategoryRepository
import cz.cvut.fel.pda.financemanagement.database.category.OfflineCategoryRepository
import cz.cvut.fel.pda.financemanagement.database.operation.loan.LoanRepository
import cz.cvut.fel.pda.financemanagement.database.operation.loan.OfflineLoanRepository
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.OfflineTransactionRepository
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.TransactionRepository
import cz.cvut.fel.pda.financemanagement.datastore.PreferencesRepository

interface AppContainer {
    val transactionRepository: TransactionRepository
    val categoryRepository: CategoryRepository
    val preferencesRepository: PreferencesRepository
    val loanRepository: LoanRepository
    val accountRepository: AccountRepository
}

class AppDataContainer(private val context: Context) : AppContainer {
    override val transactionRepository: TransactionRepository by lazy {
        OfflineTransactionRepository(AppDatabase.getDatabase(context).transactionDao())
    }
    override val categoryRepository: CategoryRepository by lazy {
        OfflineCategoryRepository(AppDatabase.getDatabase(context).categoryDao())
    }
    override val preferencesRepository: PreferencesRepository by lazy {
        PreferencesRepository(context)
    }
    override val loanRepository: LoanRepository by lazy {
        OfflineLoanRepository(AppDatabase.getDatabase(context).loanDao())
    }
    override val accountRepository: AccountRepository by lazy {
        OfflineAccountRepository(AppDatabase.getDatabase(context).accountDao())
    }
}