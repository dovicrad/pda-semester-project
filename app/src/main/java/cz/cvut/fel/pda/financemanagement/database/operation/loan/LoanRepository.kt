package cz.cvut.fel.pda.financemanagement.database.operation.loan

import kotlinx.coroutines.flow.Flow

interface LoanRepository {
    fun getAllLoansStream(): Flow<List<Loan>>
    fun getLoanForIdStream(id: Long): Flow<Loan?>
    suspend fun insertLoans(vararg loans: Loan)
    suspend fun deleteLoan(loan: Loan)
    suspend fun deleteAllLoans()
    suspend fun updateLoans(vararg loans: Loan)
}