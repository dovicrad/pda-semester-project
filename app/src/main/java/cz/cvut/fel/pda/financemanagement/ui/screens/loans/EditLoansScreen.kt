package cz.cvut.fel.pda.financemanagement.ui.screens.loans

import android.Manifest
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Close
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SegmentedButton
import androidx.compose.material3.SegmentedButtonDefaults
import androidx.compose.material3.SingleChoiceSegmentedButtonRow
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.database.operation.loan.Loan
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.Locale

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Composable
fun EditLoansScreen(
    navController: NavHostController, loanViewModel: LoanDetailViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    )
) {
    val loanUiState by loanViewModel.uiState.collectAsStateWithLifecycle()

    when (loanUiState) {
        is LoanUiState.Error -> {
            ShowErrorScreen(errorMessage = (loanUiState as LoanUiState.Error).exception.message
                ?: "Error",
                onClick = { navController.popBackStack() })
        }

        is LoanUiState.Loading -> {
            ShowLoadingScreen()
        }

        is LoanUiState.Success -> {
            val loan = (loanUiState as LoanUiState.Success).loan
            ShowSuccessScreen(
                loan = loan,
                navController = navController,
                loanViewModel = loanViewModel,
            )
        }
    }
}

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@OptIn(ExperimentalMaterial3Api::class, ExperimentalPermissionsApi::class)
@Composable
fun SetupTopBar(
    navController: NavHostController,
    loanViewModel: LoanDetailViewModel,
    amount: MutableState<String>,
    currency: MutableState<OperationCurrency>,
    type: MutableState<CategoryType>,
    receiver: MutableState<String>,
    note: MutableState<String>,
    returnDate: MutableState<LocalDateTime>
) {
    val context = LocalContext.current
    val notificationPermissionsState = rememberMultiplePermissionsState(
        permissions = listOf(
            Manifest.permission.POST_NOTIFICATIONS,
            Manifest.permission.SET_ALARM,
        )
    )
    var shouldShowRationaleDialog by remember { mutableStateOf(true) }

    if (notificationPermissionsState.shouldShowRationale && shouldShowRationaleDialog) {
        AlertDialog(onDismissRequest = {},
            title = { Text("Notification Permissions Request") },
            text = { Text("Notification permissions are necessary for posting notifications.") },
            confirmButton = {
                TextButton(onClick = { shouldShowRationaleDialog = false }) {
                    Text("OK")
                }
            })
    } else {
        LaunchedEffect(Unit) {
            notificationPermissionsState.launchMultiplePermissionRequest()
        }
    }

    TopAppBar(title = { Text("Edit Loan") },
        colors = TopAppBarDefaults.mediumTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        ),
        navigationIcon = {
            IconButton(onClick = { navController.popBackStack() }) {
                Icon(imageVector = Icons.Outlined.Close, contentDescription = "Close")
            }
        },
        actions = {
            TextButton(
                onClick = {
                    loanViewModel.saveLoan(
                        amount = BigDecimal(amount.value),
                        currency = currency.value,
                        type = type.value,
                        isReturned = false,
                        recipient = receiver.value,
                        note = note.value,
                        returnDate = returnDate.value,
                        context = context
                    )
                    navController.popBackStack()
                }, modifier = Modifier.padding(16.dp)
            ) {
                Text(text = "Save")
            }
        })
}

@OptIn(ExperimentalMaterial3Api::class)
@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Composable
fun ShowSuccessScreen(
    loan: Loan,
    navController: NavHostController,
    loanViewModel: LoanDetailViewModel,
) {
    val amount = rememberSaveable { mutableStateOf(loan.amount.toString()) }
    val currency = rememberSaveable { mutableStateOf(loan.currency) }
    val type = remember { mutableStateOf(loan.type) }
    val receiver = remember { mutableStateOf(loan.recipient) }
    val note = remember { mutableStateOf(loan.description) }
    val returnDate = remember { mutableStateOf(loan.returnDate) }

    val showCurrencyDropdown = remember { mutableStateOf(false) }

    val spaceModifier = Modifier.padding(16.dp, 4.dp)
    val options = listOf(CategoryType.INCOME, CategoryType.EXPENSE)



    Scaffold(topBar = {
        SetupTopBar(
            navController = navController,
            loanViewModel = loanViewModel,
            amount = amount,
            currency = currency,
            type = type,
            receiver = receiver,
            note = note,
            returnDate = returnDate
        )
    }, content = { innerPadding ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(innerPadding),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            SingleChoiceSegmentedButtonRow {
                options.forEachIndexed { index, option ->
                    SegmentedButton(
                        selected = type.value == option, onClick = {
                            type.value = option
                        }, shape = SegmentedButtonDefaults.itemShape(
                            index = index, count = options.size
                        )
                    ) {
                        Text(text = option.name.lowercase(Locale.getDefault()).replaceFirstChar {
                            it.titlecase(
                                Locale.getDefault()
                            )
                        })
                    }
                }
            }
            OutlinedTextField(value = amount.value, onValueChange = { newValue ->
                if (newValue.all { it.isDigit() }) {
                    amount.value = newValue
                }
            }, label = { Text("Amount") }, trailingIcon = {
                IconButton(onClick = { showCurrencyDropdown.value = true }) {
                    Text(currency.value.toString())
                }
            }, modifier = spaceModifier
            )
            DropdownMenu(modifier = spaceModifier,
                expanded = showCurrencyDropdown.value,
                onDismissRequest = { showCurrencyDropdown.value = false }) {
                OperationCurrency.entries.forEachIndexed { index, operationCurrency ->
                    DropdownMenuItem(onClick = {
                        currency.value = operationCurrency
                        showCurrencyDropdown.value = false
                    }, text = {
                        Text(operationCurrency.toString())
                    })
                    if (index < OperationCurrency.entries.size - 1) {
                        HorizontalDivider()
                    }
                }
            }
            OutlinedTextField(
                value = receiver.value,
                onValueChange = { receiver.value = it },
                label = { Text("Receiver") },
                modifier = spaceModifier
            )

            OutlinedTextField(
                value = note.value,
                onValueChange = { note.value = it },
                label = { Text("Note") },
                modifier = spaceModifier
            )
        }
    })
}