package cz.cvut.fel.pda.financemanagement.ui.screens.accounts

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import cz.cvut.fel.pda.financemanagement.FinanceManagementScreens
import cz.cvut.fel.pda.financemanagement.database.account.Account
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.datastore.PreferencesRepository
import cz.cvut.fel.pda.financemanagement.model.TransactionWithCategory
import cz.cvut.fel.pda.financemanagement.ui.components.BottomNavigation
import cz.cvut.fel.pda.financemanagement.ui.components.CreateDrawerContent
import cz.cvut.fel.pda.financemanagement.ui.screens.preferences.PreferencesUiState
import cz.cvut.fel.pda.financemanagement.ui.screens.preferences.PreferencesViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.preferences.PreferencesViewModelFactory
import cz.cvut.fel.pda.financemanagement.ui.screens.transactions.CreateTopBar
import cz.cvut.fel.pda.financemanagement.ui.screens.transactions.TransactionViewModel
import cz.cvut.fel.pda.financemanagement.ui.screens.transactions.TransactionsWithCategoryUiState
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider
import cz.cvut.fel.pda.financemanagement.ui.utils.CurrencyExchangeProvider
import java.math.BigDecimal
import java.math.RoundingMode

@Composable
fun AccountsScreen(
    navController: NavHostController,
    accountsViewModel: AccountsViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    ),
    transactionViewModel: TransactionViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    ),
    preferencesViewModel: PreferencesViewModel = viewModel(
        factory = PreferencesViewModelFactory(
            PreferencesRepository(LocalContext.current)
        )
    )
) {
    val accountsUiState: AccountsUiState by accountsViewModel.accounts.collectAsStateWithLifecycle()
    val transactionsUiState: TransactionsWithCategoryUiState by transactionViewModel.transactionsWithCategory.collectAsStateWithLifecycle()
    val preferencesUiState: PreferencesUiState by preferencesViewModel.preferencesUiStateFlow.collectAsStateWithLifecycle()

    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val scope = rememberCoroutineScope()

    ModalNavigationDrawer(drawerContent = { CreateDrawerContent(navController, drawerState, scope) }, drawerState = drawerState) {
        Scaffold(
            topBar = { CreateTopBar(scope, drawerState) },
            content = { innerPadding ->
                when (preferencesUiState) {
                    is PreferencesUiState.Loading -> CircularProgressIndicator()
                    is PreferencesUiState.Error -> Text("Error: ${(preferencesUiState as PreferencesUiState.Error).exception.message}")
                    is PreferencesUiState.Success -> {
                        val preferredCurrency = (preferencesUiState as PreferencesUiState.Success).preferences.preferredCurrency
                        Column(modifier = Modifier.padding(innerPadding)) {
                            when (transactionsUiState) {
                                is TransactionsWithCategoryUiState.Loading -> CircularProgressIndicator()
                                is TransactionsWithCategoryUiState.Error -> Text("Error: ${(transactionsUiState as TransactionsWithCategoryUiState.Error).exception.message}")
                                is TransactionsWithCategoryUiState.Success -> {
                                    val transactions = (transactionsUiState as TransactionsWithCategoryUiState.Success).itemList
                                    TotalsHeader(transactions, preferredCurrency)
                                }
                            }
                            AccountsContent(accountsUiState, navController, innerPadding, accountsViewModel)
                        }
                    }
                }
            },
            floatingActionButton = {
                FloatingActionButton(onClick = {
                    navController.navigate("${FinanceManagementScreens.EditAccount.name}/0")
                }) {
                    Icon(Icons.Filled.Add, contentDescription = "Add Account")
                }
            },
            bottomBar = { BottomNavigation(navController = navController) }
        )
    }
}

@Composable
fun TotalsHeader(transactions: List<TransactionWithCategory>, preferredCurrency: OperationCurrency) {
    val repo = PreferencesRepository(LocalContext.current)
    val totalIncome = transactions.filter { it.category.type == CategoryType.INCOME }.sumOf { it.transaction.amount * BigDecimal(CurrencyExchangeProvider.getExchangeRate(it.transaction.currency.name, repo)) }
    val totalExpense = transactions.filter { it.category.type == CategoryType.EXPENSE }.sumOf { it.transaction.amount * BigDecimal(CurrencyExchangeProvider.getExchangeRate(it.transaction.currency.name, repo)) }
    val totalBalance = totalIncome - totalExpense

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
            .border(1.dp, Color.Gray, shape = RoundedCornerShape(4.dp))
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Column(horizontalAlignment = Alignment.Start) {
                Text(
                    text = "TOTAL EXPENSE",
                    style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold),
                    color = Color.Black
                )
                Text(
                    text = "-${totalExpense.setScale(2, RoundingMode.FLOOR)} ${preferredCurrency.name}",
                    style = TextStyle(fontSize = 16.sp),
                    color = Color.Red
                )
            }
            Column(horizontalAlignment = Alignment.End) {
                Text(
                    text = "TOTAL INCOME",
                    style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold),
                    color = Color.Black
                )
                Text(
                    text = "${totalIncome.setScale(2, RoundingMode.FLOOR)} ${preferredCurrency.name}",
                    style = TextStyle(fontSize = 16.sp),
                    color = Color.Green
                )
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Text(
                    text = "TOTAL BALANCE",
                    style = TextStyle(fontSize = 16.sp, fontWeight = FontWeight.Bold),
                    color = Color.Black
                )
                Text(
                    text = "${totalBalance.setScale(2, RoundingMode.FLOOR)} ${preferredCurrency.name}",
                    style = TextStyle(fontSize = 16.sp),
                    color = Color.Black
                )
            }
        }
    }
}

@Composable
fun AccountsContent(accountsState: AccountsUiState, navController: NavHostController, innerPadding: PaddingValues, accountsViewModel: AccountsViewModel) {
    when (accountsState) {
        is AccountsUiState.Loading -> CircularProgressIndicator()
        is AccountsUiState.Error -> Text("Error: ${accountsState.exception}")
        is AccountsUiState.Success -> ShowAccountsList(accountsState.itemList, navController, innerPadding, accountsViewModel)
    }
}

@Composable
fun ShowAccountsList(accounts: List<Account>, navController: NavHostController, innerPadding: PaddingValues, accountsViewModel: AccountsViewModel) {
    LazyColumn(modifier = Modifier.padding(innerPadding)) {
        items(accounts) { account ->
            AccountItem(account, navController, accountsViewModel)
        }
    }
}

@Composable
fun AccountItem(account: Account, navController: NavHostController, accountsViewModel: AccountsViewModel) {
    var showMenu by remember { mutableStateOf(false) }
    var showDialog by remember { mutableStateOf(false) }

    if (showDialog) {
        AlertDialog(
            onDismissRequest = { showDialog = false },
            title = { Text(text = "Delete this account?") },
            text = { Text("Deleting this account will also delete all transactions associated with it.") },
            confirmButton = {
                TextButton(
                    onClick = {
                        accountsViewModel.deleteAccountWithTransactions(account)
                        showDialog = false
                    }
                ) {
                    Text("Delete")
                }
            },
            dismissButton = {
                TextButton(
                    onClick = { showDialog = false }
                ) {
                    Text("Cancel")
                }
            }
        )
    }

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp)
            .border(1.dp, Color.Gray, shape = RoundedCornerShape(4.dp))
            .clickable {
                navController.navigate("${FinanceManagementScreens.AccountDetail.name}/${account.id}")
            }
            .padding(16.dp),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column {
            Text(text = account.name, style = TextStyle(fontSize = 18.sp, fontWeight = FontWeight.Bold))
            Text(
                text = "${account.balance} ${account.currency}",
                style = TextStyle(fontSize = 16.sp),
                color = if (account.balance < BigDecimal.ZERO) Color.Red else Color.Black
            )
        }
        Box {
            IconButton(onClick = { showMenu = true }) {
                Icon(Icons.Filled.MoreVert, contentDescription = "More options")
            }
            DropdownMenu(
                expanded = showMenu,
                onDismissRequest = { showMenu = false }
            ) {
                DropdownMenuItem(onClick = {
                    navController.navigate("${FinanceManagementScreens.EditAccount.name}/${account.id}")
                    showMenu = false
                }, text = {
                    Text("Edit")
                })
                DropdownMenuItem(onClick = {
                    showDialog = true
                    showMenu = false
                }, text = {
                    Text("Delete")
                })
            }
        }
    }
}
