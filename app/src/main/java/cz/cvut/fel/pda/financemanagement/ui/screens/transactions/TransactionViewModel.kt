package cz.cvut.fel.pda.financemanagement.ui.screens.transactions

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.TransactionRepository
import cz.cvut.fel.pda.financemanagement.model.TransactionWithCategory
import cz.cvut.fel.pda.financemanagement.model.common.Result
import cz.cvut.fel.pda.financemanagement.model.common.asResult
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn

class TransactionViewModel(
    transactionRepository: TransactionRepository,
) : ViewModel(){
    val transactionsWithCategory : StateFlow<TransactionsWithCategoryUiState> =
        transactionRepository
            .getTransactionsWithCategory()
            .asResult()
            .map { allTransactionsToResult ->
                when (allTransactionsToResult) {
                    is Result.Error -> TransactionsWithCategoryUiState.Error(Exception(allTransactionsToResult.exception))
                    is Result.Loading -> TransactionsWithCategoryUiState.Loading
                    is Result.Success -> TransactionsWithCategoryUiState.Success(allTransactionsToResult.data)
                }
            }.stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5000L),
                initialValue = TransactionsWithCategoryUiState.Loading
            )
}

sealed interface TransactionsWithCategoryUiState {
    data object Loading : TransactionsWithCategoryUiState
    data class Success(val itemList: List<TransactionWithCategory> = listOf()) : TransactionsWithCategoryUiState
    data class Error(val exception: Exception) : TransactionsWithCategoryUiState
}