package cz.cvut.fel.pda.financemanagement.database.account

import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.operation.transaction.Transaction
import kotlinx.coroutines.flow.Flow
import java.math.BigDecimal

interface AccountRepository {
    fun getAllAccountsStream(): Flow<List<Account>>
    fun getAccountForIdStream(id: Long): Flow<Account?>
    suspend fun insertAccounts(vararg accounts: Account)
    suspend fun deleteAccount(account: Account)
    suspend fun deleteAllAccounts()
    suspend fun updateAccounts(vararg accounts: Account)
    fun getTransactionsWithCategoryForAccount(accountId: Long): Flow<Map<Transaction, Category>>
    suspend fun deleteAccountWithTransactions(account: Account)
    suspend fun updateAccountBalance(accountId: Long, amount: BigDecimal)
}
