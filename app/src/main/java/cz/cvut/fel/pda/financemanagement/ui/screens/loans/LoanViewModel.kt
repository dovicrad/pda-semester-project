package cz.cvut.fel.pda.financemanagement.ui.screens.loans

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fel.pda.financemanagement.database.operation.loan.Loan
import cz.cvut.fel.pda.financemanagement.database.operation.loan.LoanRepository
import cz.cvut.fel.pda.financemanagement.model.common.Result
import cz.cvut.fel.pda.financemanagement.model.common.asResult
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn

class LoanViewModel(
    loanRepository: LoanRepository,
) : ViewModel(){
    val loans : StateFlow<LoansUiState> =
        loanRepository
            .getAllLoansStream()
            .asResult()
            .map { allLoansToResult ->
                when (allLoansToResult) {
                    is Result.Error -> LoansUiState.Error(Exception(allLoansToResult.exception))
                    is Result.Loading -> LoansUiState.Loading
                    is Result.Success -> LoansUiState.Success(allLoansToResult.data)
                }
            }.stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5000L),
                initialValue = LoansUiState.Loading
            )
}

sealed interface LoansUiState {
    data object Loading : LoansUiState
    data class Success(val itemList: List<Loan> = listOf()) : LoansUiState
    data class Error(val exception: Exception) : LoansUiState
}