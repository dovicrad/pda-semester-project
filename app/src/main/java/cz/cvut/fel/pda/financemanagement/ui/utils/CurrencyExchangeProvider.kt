package cz.cvut.fel.pda.financemanagement.ui.utils

import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.datastore.PreferencesRepository
import cz.cvut.fel.pda.financemanagement.model.CurrencyRate
import kotlinx.coroutines.runBlocking
import java.io.IOException

// Step 3: Update the updateCurrencyRates method in the CurrencyExchangeProvider class
object CurrencyExchangeProvider {
    private var currencyRates: List<CurrencyRate> = emptyList()
    private var lastUpdated = System.currentTimeMillis() - 1000 * 60 * 60
    private var lastUpdatedCurrency = OperationCurrency.CZK
    private suspend fun updateCurrencyRates(repo: PreferencesRepository): Result<Boolean> {
        val currencyExchangeClient = CurrencyExchangeClient.getCurrencyExchangeClient()
        val preferredCurrency = repo.getUserPreferences().preferredCurrency

        if (lastUpdatedCurrency == preferredCurrency && lastUpdated > System.currentTimeMillis() - 1000 * 60 * 5) {
            return Result.success(true)
        }
        val response = currencyExchangeClient.getCurrency(preferredCurrency.name)

        return if (response.isSuccessful) {
            lastUpdated = System.currentTimeMillis()
            lastUpdatedCurrency = preferredCurrency
            response.body()?.let { currencyResponse ->
                currencyRates = currencyResponse.rates.map { (currencyName, rate) ->
                    CurrencyRate(currencyName, 1 / rate)
                }
                Result.success(true)
            } ?: Result.failure(IOException("Currency response is null"))
        } else {
            Result.failure(IOException("Failed to fetch currency rates from web"))
        }
    }

    fun getExchangeRate(currency: String, repo: PreferencesRepository): Double {
        runBlocking {
            updateCurrencyRates(repo)
        }
        return currencyRates.find { it.currencyName == currency }?.rate ?: 1.0
    }
}