package cz.cvut.fel.pda.financemanagement.ui.screens.transactions

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import cz.cvut.fel.pda.financemanagement.FinanceManagementScreens
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.model.TransactionWithCategory
import cz.cvut.fel.pda.financemanagement.ui.components.BottomNavigation
import cz.cvut.fel.pda.financemanagement.ui.components.ShowErrorScreen
import cz.cvut.fel.pda.financemanagement.ui.components.ShowLoadingScreen
import cz.cvut.fel.pda.financemanagement.ui.components.TopBar
import cz.cvut.fel.pda.financemanagement.ui.utils.AppViewModelProvider
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle


@Composable
fun TransactionDetailScreen(
    navController: NavHostController,
    transactionId: Long?,
    transactionViewModel: TransactionDetailViewModel = viewModel(
        factory = AppViewModelProvider.Factory
    )
){
    transactionViewModel.setTransactionId(transactionId ?: 0L)
    val transactionUiState: TransactionWithCategoryUiState by transactionViewModel.uiState.collectAsStateWithLifecycle()

    Scaffold (
        topBar = {TopBar(navController)},
        content = {
            innerPadding ->
            when (transactionUiState) {
                is TransactionWithCategoryUiState.Loading -> {
                    ShowLoadingScreen()
                }
                is TransactionWithCategoryUiState.Error -> {
                    ShowErrorScreen(
                        errorMessage = (transactionUiState as TransactionWithCategoryUiState.Error).exception.message ?: "Error",
                    )
                }
                is TransactionWithCategoryUiState.Success -> {
                    TransactionScreenContent( (transactionUiState as TransactionWithCategoryUiState.Success).transaction, innerPadding)
                }
            }
        },
        floatingActionButton = {
            FloatingActionButton(onClick = {
                navController.navigate("${FinanceManagementScreens.EditTransaction.name}/${transactionId}")
            }) {
                Icon(Icons.Default.Edit, contentDescription = "Edit Transaction")
            }
        },
        bottomBar = {
            BottomNavigation(navController = navController)
        }
    )
}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun TransactionScreenContent(transaction: TransactionWithCategory, innerPadding: PaddingValues) {
    val spacer = Modifier.padding(16.dp, 4.dp)
    Column(modifier = Modifier.padding(innerPadding)) {
        Text(text = "Transaction Details:", modifier = spacer, style= androidx.compose.ui.text.TextStyle(
            fontSize = 24.sp,
            fontWeight = FontWeight.Bold
        )
        )
        Spacer(modifier = Modifier.height(16.dp))
        Row(modifier = spacer) {
            Text(text = transaction.category.name, style = androidx.compose.ui.text.TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight.Bold
            ))
        }
        Row(modifier = spacer) {
            Text(text = "Created: ")
            Text(text = transaction.transaction.dateCreated.format(DateTimeFormatter.ofLocalizedDate(
                FormatStyle.MEDIUM)))
        }
        val textColor = if (transaction.category.type == CategoryType.EXPENSE) Color.Red else Color.Green

        Row(modifier = spacer.fillMaxWidth(), horizontalArrangement = Arrangement.End){
            Text(text = "${transaction.transaction.amount} ${transaction.transaction.currency.name}"
                , color = textColor
                , style = androidx.compose.ui.text.TextStyle(
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold
                )
            )
        }
        GlideImage(
            model = transaction.transaction.imageUrl,
            contentDescription = "Captured image",
            contentScale = ContentScale.Fit,
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(1f)
        )
    }
}

