package cz.cvut.fel.pda.financemanagement.ui.screens.loans

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.database.operation.loan.Loan
import cz.cvut.fel.pda.financemanagement.database.operation.loan.LoanRepository
import cz.cvut.fel.pda.financemanagement.ui.utils.LoanNotificationReceiver
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.concurrent.TimeUnit

class LoanDetailViewModel(
    private val loanRepository: LoanRepository, // Change to LoanRepository
    savedStateHandle: SavedStateHandle
): ViewModel() {
    private var loanId: Long = savedStateHandle.get<Long>("Id")!!

    val uiState: StateFlow<LoanUiState> =
        loanRepository
            .getLoanForIdStream(loanId) // Change to getLoanWithCategoryForIdStream
            .map {
                if (it != null) {
                    LoanUiState.Success(it)
                } else if (loanId == 0L) {
                    LoanUiState.Success(
                        Loan()
                    )
                } else {
                    LoanUiState.Error(Exception("Loan not found"))
                }
            }.stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5000L),
                initialValue = LoanUiState.Loading
            )
    fun setLoanId(id: Long) {
        loanId = id
    }
    fun saveLoan(
        amount: BigDecimal,
        currency: OperationCurrency,
        isReturned: Boolean,
        context: Context,
        type: CategoryType,
        recipient: String,
        note: String,
        returnDate: LocalDateTime
    ) {
        viewModelScope.launch {
            loanRepository.insertLoans(
                Loan(
                    id = loanId,
                    amount = amount,
                    currency = currency,
                    isReturned = isReturned,
                    type = type,
                    recipient = recipient,
                    description = note,
                    returnDate = returnDate,
                    dateCreated = LocalDateTime.now()
                )
            )
            cancelLoanReminder(context)
            scheduleLoanReminder(context)
        }
    }
}
private fun scheduleLoanReminder(context: Context) {
    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    val intent = Intent(context, LoanNotificationReceiver::class.java)
    val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_IMMUTABLE)

    val intervalMillis = TimeUnit.DAYS.toMillis(7)
    alarmManager.setRepeating(
        AlarmManager.RTC_WAKEUP,
        intervalMillis,
        intervalMillis,
        pendingIntent
    )
}
private fun cancelLoanReminder(context: Context) {
    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    val intent = Intent(context, LoanNotificationReceiver::class.java)
    val pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_IMMUTABLE)

    // Cancel the alarm
    alarmManager.cancel(pendingIntent)
}

sealed interface LoanUiState {
    data object Loading : LoanUiState
    data class Success(val loan: Loan) : LoanUiState
    data class Error(val exception: Exception) : LoanUiState
}