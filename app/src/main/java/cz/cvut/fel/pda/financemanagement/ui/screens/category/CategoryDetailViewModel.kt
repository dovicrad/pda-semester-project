package cz.cvut.fel.pda.financemanagement.ui.screens.category

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.cvut.fel.pda.financemanagement.database.category.Category
import cz.cvut.fel.pda.financemanagement.database.category.CategoryRepository
import cz.cvut.fel.pda.financemanagement.database.category.CategoryType
import cz.cvut.fel.pda.financemanagement.model.CategoryWithTransactions
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class CategoryDetailViewModel(
    private val categoryRepository: CategoryRepository,
    savedStateHandle: SavedStateHandle
): ViewModel() {
    private var categoryId: Long = savedStateHandle.get<Long>("Id")!!

    val uiState: StateFlow<CategoryDetailUiState> =
        categoryRepository
            .getCategoryWithTransactionsForIdStream(categoryId)
            .map {
                if (it != null) {
                    CategoryDetailUiState.Success(it)
                } else if (categoryId == 0L) {
                    val category = Category(
                        id = 0,
                        name = "",
                        type = CategoryType.EXPENSE
                    )
                    CategoryDetailUiState.Success(
                        CategoryWithTransactions(
                            category = category,
                            transaction = emptyList()
                        )
                    )
                } else {
                    CategoryDetailUiState.Error(Exception("Category not found"))
                }
            }.stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5000L),
                initialValue = CategoryDetailUiState.Loading
            )
    fun setCategoryId(id: Long) {
        categoryId = id
    }
    fun saveCategory(
       type: CategoryType,
       name: String,
    ) {
        viewModelScope.launch {
            categoryRepository.insertCategories(
                Category(
                    id = categoryId,
                    type = type,
                    name = name
                )
            )
        }
    }
}

sealed interface CategoryDetailUiState {
    data object Loading : CategoryDetailUiState
    data class Success(val category: CategoryWithTransactions) : CategoryDetailUiState
    data class Error(val exception: Exception) : CategoryDetailUiState
}