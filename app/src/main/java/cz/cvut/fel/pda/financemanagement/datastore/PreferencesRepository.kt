package cz.cvut.fel.pda.financemanagement.datastore

import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.model.Preferences
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import java.io.IOException

private val Context.dataStore: DataStore<androidx.datastore.preferences.core.Preferences> by preferencesDataStore(name = "preferences")

class PreferencesRepository(
    private val context: Context
) {
    private val TAG: String = "PreferencesRepository"

    private object PreferencesKeys {
        val PREFERRED_CURRENCY = stringPreferencesKey("preferred_currency")
        val SEND_NOTIFICATIONS = stringPreferencesKey("send_notifications")
    }

    val preferencesFlow: Flow<Preferences> = context.dataStore.data
        .catch { exception ->
            if (exception is IOException) {
                Log.e(TAG, "Error reading preferences.", exception)
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map { preferences ->
            mapPreference(preferences)
        }

    suspend fun updatePreferredCurrency(newPreferredCurrency: OperationCurrency) {
        context.dataStore.edit { preferences ->
            preferences[PreferencesKeys.PREFERRED_CURRENCY] = newPreferredCurrency.name
        }
    }

    suspend fun updateSendNotifications(sendNotifications: Boolean) {
        context.dataStore.edit { preferences ->
            preferences[PreferencesKeys.SEND_NOTIFICATIONS] = sendNotifications.toString()
        }
    }
    suspend fun updatePreferences(
        newPreferredCurrency: OperationCurrency,
        sendNotifications: Boolean
    ){
        context.dataStore.edit { preferences ->
            preferences[PreferencesKeys.PREFERRED_CURRENCY] = newPreferredCurrency.name
            preferences[PreferencesKeys.SEND_NOTIFICATIONS] = sendNotifications.toString()
        }
    }
    suspend fun getUserPreferences(): Preferences {
        return preferencesFlow.first()
    }
    private fun mapPreference(preferences: androidx.datastore.preferences.core.Preferences): Preferences {
        val preferredCurrency = preferences[PreferencesKeys.PREFERRED_CURRENCY] ?: OperationCurrency.CZK.name
        return Preferences(OperationCurrency.valueOf(preferredCurrency))
    }
}