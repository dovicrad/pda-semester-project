package cz.cvut.fel.pda.financemanagement.database.operation.loan

import kotlinx.coroutines.flow.Flow

class OfflineLoanRepository(private val loanDao: LoanDao):
    LoanRepository {
    override fun getAllLoansStream(): Flow<List<Loan>> = loanDao.getAllLoans()
    override fun getLoanForIdStream(id: Long): Flow<Loan?> = loanDao.getLoanForId(id)
    override suspend fun insertLoans(vararg loans: Loan) = loanDao.insertLoans(*loans)
    override suspend fun deleteLoan(loan: Loan) = loanDao.delete(loan)
    override suspend fun deleteAllLoans() = loanDao.deleteAll()
    override suspend fun updateLoans(vararg loans: Loan) = loanDao.updateLoans(*loans)
}