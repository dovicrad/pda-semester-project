package cz.cvut.fel.pda.financemanagement

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import cz.cvut.fel.pda.financemanagement.ui.screens.accounts.AccountDetailScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.accounts.AccountsScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.accounts.EditAccountScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.category.CategoriesScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.category.CategoryDetailScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.category.EditCategoryScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.loans.EditLoansScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.loans.LoanDetailScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.loans.LoansScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.preferences.NotificationsScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.preferences.PreferencesScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.transactions.EditTransactionScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.transactions.TransactionDetailScreen
import cz.cvut.fel.pda.financemanagement.ui.screens.transactions.TransactionsScreen


enum class FinanceManagementScreens {
    Transactions,
    TransactionsDetail,
    EditTransaction,

    Categories,
    CategoryDetail,
    EditCategory,

    Accounts,
    AccountDetail,
    EditAccount,

    Loans,
    LoansDetail,
    EditLoans,

    Preferences,
    Notifications
}

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
@Composable
fun FinanceManagementApp() {
    val navController: NavHostController = rememberNavController()

    NavHost(
        navController = navController,
        startDestination = FinanceManagementScreens.Transactions.name,
        modifier = Modifier
    ) {
        composable(route = FinanceManagementScreens.Transactions.name) {
            TransactionsScreen(navController)
        }
        composable(route = FinanceManagementScreens.Accounts.name) {
            AccountsScreen(navController)
        }
        composable(route = FinanceManagementScreens.Loans.name) {
            LoansScreen(navController)
        }
        composable(route = "${FinanceManagementScreens.LoansDetail.name}/{Id}",
            arguments = listOf(navArgument("Id") { type = NavType.LongType })){
            LoanDetailScreen(navController,loanId = it.arguments?.getLong("Id"))
        }
        composable(route = "${FinanceManagementScreens.EditLoans.name}/{Id}",
            arguments = listOf(navArgument("Id") { type = NavType.LongType })){
            EditLoansScreen(navController)
        }
        composable(route = FinanceManagementScreens.Categories.name) {
            CategoriesScreen(navController)
        }
        composable(route = "${FinanceManagementScreens.EditTransaction.name}/{Id}",
            arguments = listOf(navArgument("Id") { type = NavType.LongType })) {
            EditTransactionScreen(navController,
                it.arguments?.getLong("Id")
            )
        }
        composable(route = "${FinanceManagementScreens.TransactionsDetail.name}/{Id}",
            arguments = listOf(navArgument("Id") { type = NavType.LongType })) {
            TransactionDetailScreen(navController,
                transactionId = it.arguments?.getLong("Id")
            )
        }
        composable(route = "${FinanceManagementScreens.EditCategory.name}/{Id}",
            arguments = listOf(navArgument("Id") { type = NavType.LongType })) {
            EditCategoryScreen(navController,
                it.arguments?.getLong("Id")
            )
        }
        composable(route = "${FinanceManagementScreens.CategoryDetail.name}/{Id}",
            arguments = listOf(navArgument("Id") { type = NavType.LongType })) {
            CategoryDetailScreen(navController = navController,
                categoryId = it.arguments?.getLong("Id")
            )
        }
        composable(route = FinanceManagementScreens.Preferences.name) {
            PreferencesScreen(navController)
        }
        composable(route = FinanceManagementScreens.Notifications.name) {
            NotificationsScreen(navController)
        }
        composable(route = "${FinanceManagementScreens.EditAccount.name}/{Id}",
            arguments = listOf(navArgument("Id") { type = NavType.LongType })) {
            EditAccountScreen(
                navController,
                it.arguments?.getLong("Id")
            )
        }
        composable(route = "${FinanceManagementScreens.AccountDetail.name}/{Id}",
            arguments = listOf(navArgument("Id") { type = NavType.LongType })){
            AccountDetailScreen(navController = navController,
                accountId = it.arguments?.getLong("Id")
            )
        }
    }
}
