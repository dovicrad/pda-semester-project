package cz.cvut.fel.pda.financemanagement.ui.screens.preferences

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import cz.cvut.fel.pda.financemanagement.database.operation.OperationCurrency
import cz.cvut.fel.pda.financemanagement.datastore.PreferencesRepository
import cz.cvut.fel.pda.financemanagement.model.Preferences
import cz.cvut.fel.pda.financemanagement.model.common.Result
import cz.cvut.fel.pda.financemanagement.model.common.asResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

class PreferencesViewModel (
    private val preferencesRepository: PreferencesRepository
) : ViewModel() {

    val preferencesUiStateFlow: StateFlow<PreferencesUiState> =
        createPreferencesUiStateFlow(
            preferencesRepository
        )
            .stateIn(
                scope = viewModelScope,
                started = SharingStarted.WhileSubscribed(5_000),
                initialValue = PreferencesUiState.Loading,
            )

    private fun createPreferencesUiStateFlow(
        preferencesRepository: PreferencesRepository,
    ): Flow<PreferencesUiState> {
        return preferencesRepository.preferencesFlow
            .asResult()
            .map { preferencesToResult ->
                when (preferencesToResult) {
                    is Result.Error -> PreferencesUiState.Error(Exception(preferencesToResult.exception))
                    is Result.Loading -> PreferencesUiState.Loading
                    is Result.Success -> PreferencesUiState.Success(preferencesToResult.data)
                }
            }
    }
    fun saveCurrencyPreference(
        currency: OperationCurrency
    ){
        viewModelScope.launch {
            preferencesRepository.updatePreferredCurrency(
                newPreferredCurrency = currency
            )
        }
    }
    fun saveNotificationPreference(
        sendNotifications: Boolean
    ){
        viewModelScope.launch {
            preferencesRepository.updateSendNotifications(
                sendNotifications = sendNotifications
            )
        }
    }
    fun savePreferences(
        currency: OperationCurrency,
        sendNotifications: Boolean
    ){
        viewModelScope.launch {
            preferencesRepository.updatePreferences(
                newPreferredCurrency = currency,
                sendNotifications = sendNotifications
            )
        }
    }
}

class PreferencesViewModelFactory(private val preferencesRepository: PreferencesRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PreferencesViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PreferencesViewModel(preferencesRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

sealed interface PreferencesUiState {
    data object Loading : PreferencesUiState
    data class Success(val preferences: Preferences) : PreferencesUiState
    data class Error(val exception: Exception) : PreferencesUiState
}