package cz.cvut.fel.pda.financemanagement.database.operation.transaction

import cz.cvut.fel.pda.financemanagement.model.CategoryWithTransactions
import cz.cvut.fel.pda.financemanagement.model.TransactionWithCategory
import kotlinx.coroutines.flow.Flow

class OfflineTransactionRepository(private val transactionDao: TransactionDao):
    TransactionRepository {
    override fun getAllTransactionsStream(): Flow<List<Transaction>> = transactionDao.getAllTransactions()
    override fun getTransactionForIdStream(id: Long): Flow<Transaction?> = transactionDao.getTransactionForId(id)
    override fun getTransactionWithCategoryForIdStream(id: Long): Flow<TransactionWithCategory> = transactionDao.getCategoryWithTransactionForId(id)

    override fun getCategoryWithTransactions(): Flow<List<CategoryWithTransactions>> = transactionDao.getCategoryWithTransaction()
    override fun getTransactionsWithCategory(): Flow<List<TransactionWithCategory>> = transactionDao.getTransactionWithCategory()
    override suspend fun insertTransactions(vararg transactions: Transaction) = transactionDao.insertTransactions(*transactions)
    override suspend fun deleteTransaction(transaction: Transaction) = transactionDao.delete(transaction)
    override suspend fun deleteAllTransactions() = transactionDao.deleteAll()
    override suspend fun deleteTransactionsByAccountId(accountId: Long) = transactionDao.deleteTransactionsByAccountId(accountId)
    override suspend fun updateTransactions(vararg transactions: Transaction) = transactionDao.updateTransactions(*transactions)
}